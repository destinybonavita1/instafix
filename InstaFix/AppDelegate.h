//
//  AppDelegate.h
//  InstaFix
//
//  Created by Destiny Dawn on 11/17/13.
//  Copyright (c) 2013 Young and Strong Productions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) UIWindow *window;

+ (UIStoryboard *)storyboard;

@end
