//
//  SocialImageCollectionViewController.h
//  InstaFix
//
//  Created by Destiny Dawn on 12/27/14.
//  Copyright (c) 2014 Young and Strong Productions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhotoSelectorDelegate.h"

@interface SocialImageCollectionViewController : UICollectionViewController

+ (instancetype)instance:(SocialNetwork)network;

@property (nonatomic) SocialNetwork network;
@property (nonatomic, retain) NSString *cursor;
@property (nonatomic, retain) NSMutableArray *urls;
@property (nonatomic, retain) id<PhotoSelectorDelegate> delegate;

- (void)loadInstagramCursor;
- (void)loadFacebookCursor;

@end