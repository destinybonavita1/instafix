//
//  MISViewCell.h
//  InstaFix
//
//  Created by Destiny Dawn on 11/20/13.
//  Copyright (c) 2013 Young and Strong Productions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MISViewCell : UICollectionViewCell

@property (nonatomic) BOOL isSelected;
@property (nonatomic) BOOL isLoadMore;

@property (strong, nonatomic) IBOutlet UILabel *loadMoreLbl;
@property (strong, nonatomic) IBOutlet UIImageView *imageDisplay;

- (void)setPhoto:(UIImage *)photo;

@end
