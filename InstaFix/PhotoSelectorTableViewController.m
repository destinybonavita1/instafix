//
//  PhotoSelectorTableViewController.m
//  InstaFix
//
//  Created by Destiny Dawn on 12/27/14.
//  Copyright (c) 2014 Young and Strong Productions. All rights reserved.
//

#import "Settings.h"
#import "Facebook.h"
#import "Instagram.h"
#import "PhotoSelectorTableViewController.h"
#import "SocialImageCollectionViewController.h"
#import "CustomImageSelectorTableViewController.h"

@interface PhotoSelectorTableViewController ()

@property (nonatomic, retain) NSArray *features;
//@property (nonatomic, retain) NSArray *titles;

@end

@implementation PhotoSelectorTableViewController

#pragma mark - UIView Life Cycle

- (void)viewDidLoad {
    self.features = @[@[@"Photo Gallery", @"Facebook"],
                      @[@"Auto Load All", @"Auto Position"]];
    self.tableView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.jpg"]];
    
    [self.tableView reloadData];
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    // Dispose of any resources that can be recreated.
    [super didReceiveMemoryWarning];
}

#pragma mark - Private

- (void)cancelPhotoSelector
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
//{
//    return self.titles[section];
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [self.features[section] count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"featuresCell"];
    if (!cell) cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"featuresCell"];
    
    // Configure the cell...
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.font = [UIFont systemFontOfSize:30.0f];
    
    cell.textLabel.text = self.features[indexPath.section][indexPath.row];
    cell.backgroundColor = [UIColor clearColor];
    
    UIVisualEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
    UIVisualEffectView *effectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    cell.backgroundView = effectView;
    
    //Displays settings
    if (indexPath.section == 1)
    {
        Settings *settings = [Settings instance];
        switch (indexPath.row)
        {
            case 0:
                cell.accessoryType = (settings.autoLoad) ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
                break;
            case 1:
                cell.accessoryType = (settings.autoFormat) ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
                break;
        }
        
        if (![Settings instance].tutorialGrabAll) {
            [Settings instance].tutorialGrabAll = YES;
            [self displayTutorialWithMessage:@"Press this automatically load all pictures from the selected social network." aboveView:cell];
        }
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 74;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *value = self.features[indexPath.section][indexPath.row];
    
    //Local
    if ([value isEqualToString:@"Camera"])
    {
//#warning Camera not finished
//        exit(0);
    }
    else if ([value isEqualToString:@"Photo Gallery"])
    {
        CustomImageSelectorTableViewController *vc = [[CustomImageSelectorTableViewController alloc] initWithStyle:UITableViewStyleGrouped];
        [vc view];
        
        vc.navigationItem.title = value;
        vc.delegate = self.delegate;
        
        [self.navigationController pushViewController:vc animated:YES];
    }
    
    //Social
    else if ([value isEqualToString:@"Instagram"])
    {
        InstagramAuth *vc = [[InstagramAuth alloc] init];
        vc.navigationItem.title = @"Instagram";
        vc.delegate = self.delegate;
        
        [vc view];
        [self.navigationController pushViewController:vc animated:YES];
    }
    else if ([value isEqualToString:@"Facebook"])
    {
        self.tableView.userInteractionEnabled = NO;
        if ([Facebook instance].isSignedIn)
            [self displayFacebookPhotos];
        else
        {
            [[Facebook instance] signIn:^(FBSession *session, FBSessionState status, NSError *error) {
                if ([Facebook instance].isSignedIn)
                    [self displayFacebookPhotos];
                else
                    [FBSession.activeSession closeAndClearTokenInformation];
                
                self.tableView.userInteractionEnabled = YES;
            }];
        }
    }
    
    //Settings
    else if ([value isEqualToString:@"Auto Load All"]) {
        [Settings instance].autoLoad = ![Settings instance].autoLoad;
        [tableView reloadData];
    }
    else if ([value isEqualToString:@"Auto Position"]) {
        [Settings instance].autoFormat = ![Settings instance].autoFormat;
        [tableView reloadData];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Other

- (void)displayFacebookPhotos
{
    SocialImageCollectionViewController *vc = [SocialImageCollectionViewController instance:FacebookNet];
    if (!vc.urls)
    {
        [FBRequestConnection startWithGraphPath:@"me/photos/" parameters:nil HTTPMethod:@"GET" completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
            //Re-enable the UITableView
            self.tableView.userInteractionEnabled = YES;
            
            //Run the rest
            FBGraphObject *returned = result;
            if (returned)
            {
                NSArray *photos = returned.allValues[0];
                NSString *cursor = returned.allValues[1][@"next"];
                if (cursor)
                    cursor = [cursor stringByReplacingOccurrencesOfString:@"https://graph.facebook.com/v2.2/" withString:@""];
                
                NSMutableArray *photoUrls = [[NSMutableArray alloc] init];
                for (NSDictionary *dict in photos)
                {
                    [photoUrls addObject:dict[@"images"]];
                }
                
                vc.cursor = cursor;
                vc.urls = photoUrls;
                vc.delegate = self.delegate;
                vc.navigationItem.title = @"Facebook";
                
                [vc view];
                
                if (![self.navigationController.topViewController isEqual:vc])
                    [self.navigationController pushViewController:vc animated:YES];
            }
        }];
    }
    else
    {
        self.tableView.userInteractionEnabled = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }
    
}

#pragma mark - UIImagePickerController Delegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSLog(@"%@", info);
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
