//
//  AssetLoader.m
//  InstaFix
//
//  Created by Destiny Dawn on 11/26/13.
//  Copyright (c) 2013 Young and Strong Productions. All rights reserved.
//

#import "AssetLoader.h"

@implementation AssetLoader

+ (instancetype)sharedInstance {
    static AssetLoader *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[AssetLoader alloc] init];
    });
    
    return instance;
}

- (UIImage *)loadAsset:(NSURL *)assetURL {
    CGImageRef imgRef;
    
    __block ALAsset *result = nil;
    [result retain];
    
    dispatch_semaphore_t sema = dispatch_semaphore_create(0);
    
    @autoreleasepool {
        ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
        [library assetForURL:assetURL resultBlock:^(ALAsset *asset) {
            result = [asset retain];
            dispatch_semaphore_signal(sema);
        } failureBlock:^(NSError *error) {
            //Handle error
            dispatch_semaphore_signal(sema);
        }];
    }
    
    if ([NSThread isMainThread]) {
        while (!result) {
            [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
        }
    }
    else {
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    }
    
    dispatch_release(sema);
    
    imgRef = [[result defaultRepresentation] fullScreenImage];
    
    return [UIImage imageWithCGImage:imgRef scale:[[result defaultRepresentation] scale] orientation:0];
}

- (UIImage *)loadThumbnail:(NSURL *)assetURL {
    CGImageRef imgRef;
    
    __block ALAsset *result = nil;
    [result retain];
    
    dispatch_semaphore_t sema = dispatch_semaphore_create(0);
    
    @autoreleasepool {
        ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
        [library assetForURL:assetURL resultBlock:^(ALAsset *asset) {
            result = [asset retain];
            dispatch_semaphore_signal(sema);
        } failureBlock:^(NSError *error) {
            //Handle error
            dispatch_semaphore_signal(sema);
        }];
    }
    
    if ([NSThread isMainThread]) {
        while (!result) {
            [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
        }
    }
    else {
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    }
    
    dispatch_release(sema);
    
    imgRef = [result thumbnail];
    
    return [UIImage imageWithCGImage:imgRef scale:[[result defaultRepresentation] scale] orientation:0];
}

@end
