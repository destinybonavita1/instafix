//
//  ResizingViewController.h
//  InstaFix
//
//  Created by Destiny Bonavita on 5/6/15.
//  Copyright (c) 2015 Young and Strong Productions. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ResizingViewControllerDelegate <NSObject>

@optional
- (void)resizingViewController:(UIViewController *)delegate optionSelected:(NSArray *)json;

@end

@interface ResizingViewController : UIViewController

@property (nonatomic, retain) NSArray *json;
@property (nonatomic, retain) id<ResizingViewControllerDelegate> delegate;

- (void)showInView:(UIView *)view aboveView:(UIView *)above;

@end
