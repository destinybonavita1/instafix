//
//  UITextField+Editing.h
//  InstaFix
//
//  Created by Destiny Bonavita on 5/4/15.
//  Copyright (c) 2015 Young and Strong Productions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (Editing)

@property (nonatomic) CGRect lastFrame;

- (BOOL)becomeFirstResponder:(UILabel *)label;
- (BOOL)resignFirstResponder:(UILabel *)label;

@end
