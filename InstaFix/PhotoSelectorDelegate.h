//
//  PhotoSelectorDelegate.h
//  InstaFix
//
//  Created by Destiny Dawn on 12/27/14.
//  Copyright (c) 2014 Young and Strong Productions. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PhotoSelectorDelegate <NSObject>

typedef enum : NSUInteger {
    FacebookNet,
    InstagramNet
} SocialNetwork;

- (void)photoSelectorDelegate:(id)selector imageSelected:(UIImage *)image;
- (void)photoSelectorDelegate:(id)selector imagesSelected:(NSArray *)images;

@end
