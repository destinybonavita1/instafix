//
//  CustomImageSelectorTableViewController.m
//  InstaFix
//
//  Created by Destiny Dawn on 12/27/14.
//  Copyright (c) 2014 Young and Strong Productions. All rights reserved.
//

#import <AssetsLibrary/AssetsLibrary.h>

#import "AlbumViewerCollectionViewController.h"
#import "CustomImageSelectorTableViewController.h"

@interface CustomImageSelectorTableViewController ()
{
    NSMutableArray *assetGroups;
}

@property (nonatomic, retain) NSMutableDictionary *selectedImages;

@end

@implementation CustomImageSelectorTableViewController

#pragma mark - UIView Life Cycle

- (void)viewDidLoad {
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    UIImageWriteToSavedPhotosAlbum(nil, nil, nil, nil);
    assetGroups = [[NSMutableArray alloc] init];
    
    ALAssetsLibrary *al = [[ALAssetsLibrary alloc] init];
    self.tableView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.jpg"]];
    
    //Gets all photos
    [al enumerateGroupsWithTypes:ALAssetsGroupAll usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
        if (group != nil) {
            if (![group posterImage])
                return;
            
            NSMutableArray *keys = [[NSMutableArray alloc] init];
            [keys addObject:[group valueForProperty:ALAssetsGroupPropertyName]];
            [keys addObject:[[UIImage alloc] initWithCGImage:[group posterImage]]];
            [keys addObject:[group valueForProperty:ALAssetsGroupPropertyURL]];
            
            [assetGroups addObject:keys];
        }
        
        if (group == nil) {
            [self.tableView reloadData];
        }
    } failureBlock:^(NSError *error) {
        // User did not allow access to library
        // .. handle error
    }];
    
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    // Dispose of any resources that can be recreated.
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [assetGroups count];;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"featuresCell"];
    if (!cell) cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"featuresCell"];
    
    // Configure the cell...
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.font = [UIFont systemFontOfSize:15.0];
    cell.textLabel.text = assetGroups[indexPath.row][0];
    cell.imageView.image = assetGroups[indexPath.row][1];
    cell.backgroundColor = [UIColor clearColor];
    
    UIVisualEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
    UIVisualEffectView *effectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    cell.backgroundView = effectView;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.minimumLineSpacing = 0.0f;
    layout.minimumInteritemSpacing = 0.0f;
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    
    AlbumViewerCollectionViewController *vc = [[AlbumViewerCollectionViewController alloc] initWithCollectionViewLayout:layout];
    
    vc.groupURL = assetGroups[indexPath.row][2];
    vc.navigationItem.title = assetGroups[indexPath.row][0];
    
    if (self.delegate) vc.delegate = self.delegate;
    
    [vc view];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.navigationController pushViewController:vc animated:YES];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
