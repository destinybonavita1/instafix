//
//  AspectRatioViewController.m
//  InstaFix
//
//  Created by Destiny Eclipse on 3/14/14.
//  Copyright (c) 2014 Young and Strong Productions. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "AspectRatioViewController.h"

@interface AspectRatioViewController ()

@end

@implementation AspectRatioViewController
@synthesize viewScrollView, orientationSelection;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(hideView)];
    [swipe setDirection:UISwipeGestureRecognizerDirectionDown];
    [[self view] addGestureRecognizer:swipe];
}

- (void)hideView {
    [UIView animateWithDuration:0.5f animations:^(void) {
        [[self view] setFrame:CGRectMake(0, [[UIScreen mainScreen] bounds].size.height + 200, 320, 190)];
        [[self view] setAlpha:0.0f];
    } completion:^(BOOL finished) {
        [[self view] removeFromSuperview];
        if ([[self delegate] respondsToSelector:@selector(aspectRatioControllerViewHidden:)]) {
            [[self delegate] aspectRatioControllerViewHidden:self];
        }
    }];
}

- (void)SetAspectRatioOptions {
    UISegmentedControl *con = (UISegmentedControl *)orientationSelection;
    typeOf = [con titleForSegmentAtIndex:[con selectedSegmentIndex]];
    if ([typeOf isEqualToString:@"Square"])
        return;
    
    for (UIView *v in [viewScrollView subviews]) {
        [v removeFromSuperview];
    }
    
    float pos = 0.0f;
    float lastX = 0.0f;
    
    if ([typeOf isEqualToString:@"Portrait"]) {
        for (int i = 0; i < 3; i += 1) {
            float Width = 0.0f;
            CGRect frame = CGRectMake(15 + ((pos - 0.0f) * (80 + 15)), 15, 80, 80);
            if (i == 0) {
                Width = roundf((frame.size.height / 71) * 40);
            } else if (i == 1) {
                Width = roundf((frame.size.height / 3) * 2);
            } else if (i == 2) {
                Width = roundf((frame.size.height / 4) * 3);
            }
            
            frame.size.width = Width;
            frame.origin.x  += Width * 0.5f;
            
            AspectView *view = [[AspectView alloc] initWithFrame:frame];
            [[view layer] setBorderColor:[[UIColor blackColor] CGColor]];
            [[view layer] setBorderWidth:1.0f];
            
            CGRect lblFrame = view.frame;
            UILabel *l = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, lblFrame.size.width, lblFrame.size.height)];
            if (i == 0) {
                [l setText:@"iPhone\n5"];
                [l setFont:[UIFont systemFontOfSize:10.0f]];
                [view setAspectRatio:71.40];
            } else if (i == 1) {
                [l setText:@"iPhone\n4"];
                [l setFont:[UIFont systemFontOfSize:10.0f]];
                [view setAspectRatio:3.2];
            } else if (i == 2) {
                [l setText:@"iPad"];
                [view setAspectRatio:4.3];
            }
            
            [l setNumberOfLines:999999];
            [l setTextAlignment:NSTextAlignmentCenter];
            
            [view setBackgroundColor:[UIColor whiteColor]];
            [l setTextColor:[UIColor blackColor]];
            
            pos++;
            lastX = frame.origin.x + Width;
            [view addSubview:l];
            [viewScrollView addSubview:view];
            [l setBackgroundColor:[UIColor clearColor]];
            
            UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(DidSelectAspectView:)];
            [gesture setDelegate:self];
            [view addGestureRecognizer:gesture];
        }
    } else {
        for (int i = 0; i < 3; i += 1) {
            float Width = 0.0f;
            CGRect frame = CGRectMake(lastX, 15, 80, 80);
            if (i == 0) {
                Width = roundf((frame.size.height / 40) * 71);
            } else if (i == 1) {
                Width = roundf((frame.size.height / 2) * 3);
            } else if (i == 2) {
                Width = roundf((frame.size.height / 3) * 4);
            }
            
            frame.size.width = Width;
            frame.origin.x  += Width * 0.5f;
            if (lastX == 0.0f)
                frame.origin.x  = 15.0f;
            else
                frame.origin.x -= 25.5;
            
            AspectView *view = [[AspectView alloc] initWithFrame:frame];
            [[view layer] setBorderColor:[[UIColor blackColor] CGColor]];
            [[view layer] setBorderWidth:1.0f];
            
            CGRect lblFrame = view.frame;
            UILabel *l = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, lblFrame.size.width, lblFrame.size.height)];
            if (i == 0) {
                [l setText:@"iPhone 5"];
                [view setAspectRatio:40.71];
            } else if (i == 1) {
                [l setText:@"iPhone 4"];
                [view setAspectRatio:2.3];
            } else if (i == 2) {
                [l setText:@"iPad"];
                [view setAspectRatio:3.4];
            }
            
            [l setNumberOfLines:999999];
            [l setTextAlignment:NSTextAlignmentCenter];
            
            [view setBackgroundColor:[UIColor whiteColor]];
            [l setTextColor:[UIColor blackColor]];
            
            pos++;
            lastX = frame.origin.x + Width;
            [view addSubview:l];
            [viewScrollView addSubview:view];
            [l setBackgroundColor:[UIColor clearColor]];
            
            UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(DidSelectAspectView:)];
            [gesture setDelegate:self];
            [view addGestureRecognizer:gesture];
        }
    }
    
    [viewScrollView setContentSize:CGSizeMake(lastX + 15, (80 + 15))];
}

- (IBAction)didChangeOrientation:(id)sender {
    UISegmentedControl *con = (UISegmentedControl *)sender;
    typeOf = [con titleForSegmentAtIndex:[con selectedSegmentIndex]];
    if ([typeOf isEqualToString:@"Square"]) {
        [[self delegate] aspectRatioController:self ratio:1.1f];
    }
    
    [self SetAspectRatioOptions];
    [con setSelectedSegmentIndex:0];
}

- (void)DidSelectAspectView:(UITapGestureRecognizer *)tap {
    if ([[self delegate] respondsToSelector:@selector(aspectRatioController:ratio:)]) {
        [[self delegate] aspectRatioController:self ratio:[(AspectView *)[tap view] AspectRatio]];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

@implementation AspectView



@end
