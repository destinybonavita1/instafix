//
//  ImageHandler.m
//  InstaFix Pro
//
//  Created by Destiny Dawn on 8/5/13.
//  Copyright (c) 2013 Young and Strong Productions. All rights reserved.
//

#import "ImageHandler.h"
#import "UIImageView+GestureImage.h"

@implementation ImageHandler

+ (instancetype)instance
{
    static ImageHandler *i;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        i = [[ImageHandler alloc] init];
    });
    
    return i;
}

#pragma mark - Properties

- (NSString *)imageDirectory {
    return NSTemporaryDirectory();
}

#pragma mark - Public

- (UIImage *)finalizeImage
{
    UIImage *final;
    
    @autoreleasepool {
        //Handle the array first
        for (UIView *view in self.imageView.subviews) {
            if ([view isKindOfClass:[GestureImage class]])
                [(GestureImage *)view setGenerator];
        }
        
        //Generates the new size
        CGSize newSize = [self sizeToScale:self.imageView.frame max:FinalSize];
        
        //Starts the image context
        UIGraphicsBeginImageContextWithOptions(newSize, YES, 0);
        
        //Fills the context with the selected background
        CGContextSetFillColorWithColor(UIGraphicsGetCurrentContext(), self.imageView.backgroundColor.CGColor);
        CGContextFillRect(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, newSize.width, newSize.height));
        
        for (UIView *view in self.imageView.subviews) {
            //It's a label
            if ([view isKindOfClass:[UILabel class]]) {
                UILabel *t = (UILabel *)view;
                CGRect frame = t.frame;
                float x = (frame.origin.x / self.imageView.frame.size.width);
                float p = (frame.origin.y / self.imageView.frame.size.height);
                float h = (frame.size.height / self.imageView.frame.size.height);
                frame.origin.x = newSize.width * x;
                frame.origin.y = newSize.height * p;
                frame.size.height = newSize.height * h;
                frame.size.width = newSize.width;
                
                //Generates the new font size
                CGFloat diff = (t.font.pointSize / t.frame.size.height);
                CGFloat fontSize = frame.size.height * diff;
                while ((fontSize * t.text.length) > frame.size.width)
                    fontSize -= 0.1f;
                
                //Draws the background
                [t.backgroundColor setStroke];
                [t.backgroundColor setFill];
                CGContextFillRect(UIGraphicsGetCurrentContext(), frame);
                
                //Fills in the text
                NSMutableParagraphStyle *style = [NSMutableParagraphStyle defaultParagraphStyle].mutableCopy;
                style.alignment = NSTextAlignmentCenter;
                
                [t.text drawInRect:frame withAttributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:fontSize], NSParagraphStyleAttributeName : style, NSForegroundColorAttributeName : t.textColor }];
            }
            
            //It's a image
            else if ([view isKindOfClass:[GestureImage class]]) {
                GestureImage *i = (GestureImage *)view;
                CGRect frame = i.boundingFrame;
                CGSize iSize = i.rotatedImage.size;
                float rightX = 0.0, leftX = 0.0, bottomY = 0.0, topY = 0.0;
                
                leftX = frame.origin.x;
                topY = frame.origin.y;
                
                //Image is wider than it is taller
                if (iSize.width > iSize.height) {
                    float origHeight = i.boundingFrame.size.height;
                    float diff = iSize.height / iSize.width;
                    frame.size.height = (frame.size.width * diff);
                    if (origHeight < frame.size.height)
                        frame.origin.y += (frame.size.height - origHeight) / 2;
                    else
                        frame.origin.y += (origHeight - frame.size.height) / 2;
                    
                    topY = frame.origin.y;
                    topY /= self.imageView.frame.size.height;
                    leftX /= self.imageView.frame.size.width;
                    rightX = (frame.origin.x + frame.size.width) / self.imageView.frame.size.width;
                    bottomY = (frame.origin.y + frame.size.height) / self.imageView.frame.size.height;
                }
                
                //Image is taller than it is wider
                else if (iSize.height > iSize.width) {
                    float origWidth = i.boundingFrame.size.width;
                    float diff = iSize.width / iSize.height;
                    frame.size.width = (frame.size.height * diff);
                    if (origWidth < frame.size.width)
                        frame.origin.x += (frame.size.width - origWidth) / 2;
                    else
                        frame.origin.x += (origWidth - frame.size.width) / 2;
                    
                    leftX = frame.origin.x;
                    leftX /= self.imageView.frame.size.width;
                    topY /= self.imageView.frame.size.height;
                    rightX = (frame.origin.x + frame.size.width)  / self.imageView.frame.size.width;
                    bottomY = (frame.origin.y + frame.size.height) / self.imageView.frame.size.height;
                }
                
                //Image is a square
                else {
                    leftX /= self.imageView.frame.size.width;
                    topY /= self.imageView.frame.size.height;
                    rightX = (frame.origin.x + frame.size.width)  / self.imageView.frame.size.width;
                    bottomY = (frame.origin.y + frame.size.height) / self.imageView.frame.size.height;
                }
                
                frame.origin.x = newSize.width * leftX;
                frame.origin.y = newSize.height * topY;
                
                frame.size.width = (newSize.width * rightX) - frame.origin.x;
                frame.size.height = (newSize.height * bottomY) - frame.origin.y;
                
                [i.rotatedImage drawInRect:frame];
            }
        }
        
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGContextSetInterpolationQuality(context, kCGInterpolationHigh);
        final = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:UIImagePNGRepresentation(final) forKey:@"Last_Saved"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    return final;
}

- (UIImage *)finalizedImage
{
    UIImage *final;
    
    @autoreleasepool {
        //Handle the array first
        for (UIView *view in self.imageView.subviews) {
            if ([view isKindOfClass:[GestureImage class]])
                [(GestureImage *)view setGenerator];
        }
        
        //Generates the new size
        CGSize newSize = [self sizeToScale:self.imageView.frame max:FinalSize];
        
        //Starts the image context
        UIGraphicsBeginImageContextWithOptions(newSize, YES, 0);
        
        //Fills the context with the selected background
        CGContextSetFillColorWithColor(UIGraphicsGetCurrentContext(), self.imageView.backgroundColor.CGColor);
        CGContextFillRect(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, newSize.width, newSize.height));
        
        //Gets the ImageView's Sizes
        CGFloat iWidth = CGRectGetWidth(self.imageView.frame);
        CGFloat iHeight = CGRectGetHeight(self.imageView.frame);
        
        for (UIView *view in self.imageView.subviews) {
            GestureImage *i = (GestureImage *)view;
            UIImage *image = i.rotatedImage;
            CGRect frame = i.boundingFrame;
            CGFloat x = 0.0f, y = 0.0f, width = 0.0f, height = 0.0f;
            
            //Sets the percentages
            x = frame.origin.x / iWidth;
            y = frame.origin.y / iHeight;
            width = frame.size.width / iWidth;
            height = frame.size.height / iHeight;
            
            //Sets the new values
            x = newSize.width * x;
            y = newSize.height * y;
            width = newSize.width * width;
            height = newSize.height * height;
            
            //Sets the new frame
            frame = CGRectMake(x, y, width, height);
            
            //Auto Sized
            CGFloat nWidth = ((image.size.width / image.size.height) * height);
            CGFloat nHeight = ((image.size.height / image.size.width) * width);
            
            //JSON Sized
            CGFloat nHeight2 = ((i.frame.size.height / i.frame.size.width) * width);
            CGFloat nWidth2 = ((i.frame.size.width / i.frame.size.height) * height);
            
            //Resizes the frame
            if (image.size.width > image.size.height) {
                frame.size.height = (nHeight < nHeight2) ? nHeight : nHeight2;
                frame.origin.y += (height - nHeight) / 2.0f;
            }
            else if (image.size.height > image.size.width) {
                frame.size.width = (nWidth < nWidth2) ? nWidth : nWidth2;
                frame.origin.x += (width - frame.size.width) / 2.0f;
            }
            
            [image drawInRect:frame];
        }
        
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGContextSetInterpolationQuality(context, kCGInterpolationHigh);
        final = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:UIImagePNGRepresentation(final) forKey:@"Last_Saved"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    return final;
}

- (CGSize)sizeToSquare:(CGSize)size
{
    if (size.height > size.width)
        return CGSizeMake(size.height, size.height);
    else
        return CGSizeMake(size.width, size.width);
}

#pragma mark - Private

- (CGSize)sizeToScale:(CGRect)input max:(CGSize)max
{
    CGSize newSize = CGSizeZero;
    
    if (CGRectGetHeight(input) > CGRectGetWidth(input)) {
        newSize = CGSizeMake((max.height / CGRectGetHeight(input)) * CGRectGetWidth(input), max.height);
    } else if (input.size.height == CGRectGetWidth(input)) {
        newSize = FinalSize;
    } else {
        newSize = CGSizeMake(max.height, (max.height / CGRectGetWidth(input)) * CGRectGetHeight(input));
    }
    
    return newSize;
}

#pragma mark - Static

+ (UIImage *)resizeImage:(UIImage *)image {
    return [self resizeImage:image color:[UIColor clearColor]];
}

+ (UIImage *)resizeImage:(UIImage *)image color:(UIColor *)color {
    UIImage *drawingImg;
    
    @autoreleasepool {
        float actualHeight = image.size.height;
        float actualWidth = image.size.width;
        
        if (image.size.height > image.size.width)
        {
            actualHeight = image.size.height;
            actualWidth = image.size.height;
        } else {
            actualHeight = image.size.width;
            actualWidth = image.size.width;
        }
        
        CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
        
        UIGraphicsBeginImageContext(rect.size);
        
        CGContextSetFillColorWithColor(UIGraphicsGetCurrentContext(), [UIColor clearColor].CGColor);
        [color set];
        UIRectFill(rect);
        
        CGRect orgRect = CGRectMake((actualHeight / 2) - (image.size.width / 2), (actualHeight / 2) - (image.size.height / 2), image.size.width, image.size.height);
        [image drawInRect:orgRect];
        
        drawingImg = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    
    
    return drawingImg;
}

+ (NSString *)saveImage:(UIImage *)image {
    NSString *imageName = [NSString stringWithFormat:@"Image_%u.png", arc4random() % 5000000 + 15];
    NSString *directory = [[ImageHandler instance].imageDirectory stringByAppendingPathComponent:imageName];
    
    //Saves the file
    [UIImagePNGRepresentation(image) writeToFile:directory options:NSDataWritingAtomic error:nil];
    
    //Returns the path
    return directory;
}

@end
    