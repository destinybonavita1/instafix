//
//  PhotoEditorViewController.h
//  InstaFix
//
//  Created by Destiny Dawn on 11/17/13.
//  Copyright (c) 2013 Young and Strong Productions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>
#import <Accounts/Accounts.h>
#import <MessageUI/MessageUI.h>
#import <QuartzCore/QuartzCore.h>
#import "PhotoSelectorDelegate.h"
#import "AspectRatioViewController.h"
#import "ColorSelectorViewController.h"

@class UIImageEditor;

@interface PhotoEditorViewController : UIViewController <UINavigationControllerDelegate, UIDocumentInteractionControllerDelegate, UIActionSheetDelegate, UIAlertViewDelegate, ColorSelectorDelegate, AspectRatioSelectorDelegate, PhotoSelectorDelegate> {
    BOOL isColor;
    NSObject *obj;
    BOOL isBackgroundImage;
}

@property (nonatomic, retain) AspectRatioViewController *aspectRatioSelector;
@property (nonatomic, retain) ColorSelectorViewController *colorSelector;
@property (nonatomic, retain) UIDocumentInteractionController *dic;
@property (weak, nonatomic) IBOutlet UIImageEditor *imageDisplay;
@property (weak, nonatomic) IBOutlet UIToolbar *bottomToolBar;
@property (nonatomic, readwrite) int imageCount;

- (IBAction)addText:(id)sender;
- (IBAction)addImage:(id)sender;
- (IBAction)shareImage:(id)sender;
- (IBAction)changeSettings:(id)sender;
- (IBAction)selectBackground:(id)sender;

- (void)handlePan:(UIPanGestureRecognizer *)sender;
- (void)handlePinch:(UIPinchGestureRecognizer *)sender;
- (void)handleRotate:(UIRotationGestureRecognizer *)sender;
- (void)touchAndHold:(UILongPressGestureRecognizer *)sender;

@end
