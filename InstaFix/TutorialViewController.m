//
//  TutorialViewController.m
//  InstaFix
//
//  Created by Destiny Bonavita on 5/3/15.
//  Copyright (c) 2015 Young and Strong Productions. All rights reserved.
//

#import "TutorialViewController.h"

@interface TutorialViewController ()

@property (nonatomic, weak) IBOutlet UILabel *label;

@end

@implementation TutorialViewController

#pragma mark - Static

+ (instancetype)initWithMessage:(NSString *)message
{
    TutorialViewController *tutorial = [[UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil] instantiateViewControllerWithIdentifier:@"tutorialViewController"];
    [tutorial view];
    
    tutorial.label.text = message;
    
    return tutorial;
}

#pragma mark - UIView Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIPopoverPresentationController Delegate

- (UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller {
    return UIModalPresentationNone;
}

- (void)popoverPresentationControllerDidDismissPopover:(UIPopoverPresentationController *)popoverPresentationController
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kTutorialPopoverDimissed object:nil];
}

@end
