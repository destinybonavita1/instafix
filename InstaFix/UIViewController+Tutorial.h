//
//  UIViewController+Tutorial.h
//  InstaFix
//
//  Created by Destiny Bonavita on 5/3/15.
//  Copyright (c) 2015 Young and Strong Productions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Tutorial)

- (void)displayTutorialWithMessage:(NSString *)message aboveView:(UIView *)view;

@end
