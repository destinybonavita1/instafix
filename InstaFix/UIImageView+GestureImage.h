//
//  UIImageView+GestureImage.h
//  InstaFix
//
//  Created by Destiny Eclipse on 3/14/14.
//  Copyright (c) 2014 Young and Strong Productions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GestureImage :UIImageView;

@property (nonatomic, readwrite) BOOL PositionLock;

@property (nonatomic, retain) UIImage *rotatedImage;
@property (nonatomic, readonly) CGRect boundingFrame;

- (void)setGenerator;

@end
