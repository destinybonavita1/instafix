//
//  ColorSelectorViewController.h
//  InstaFix
//
//  Created by Destiny Dawn on 11/3/13.
//  Copyright (c) 2013 Young and Strong Productions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
// 

@protocol ColorSelectorDelegate <NSObject>

@optional
- (void)ViewHidden;
- (void)colorChange:(UIColor *)color;

@end

@interface ColorSelectorViewController : UIViewController <UIGestureRecognizerDelegate> {
    NSString *typeOf;
}

@property (nonatomic, retain) id<ColorSelectorDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIScrollView *colorView;
@property (nonatomic, readwrite) NSMutableArray *favoriteBackgrounds;
@property (weak, nonatomic) IBOutlet UISegmentedControl *rgbSegmented;

- (void)hideView:(BOOL)remove;
- (void)showView;

- (void)setColorViewChoices;
- (IBAction)changeRGB:(id)sender;
- (void)showInView:(UIView *)view aboveView:(UIView *)above;

@end
