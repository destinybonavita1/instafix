//
//  UITextField+Editing.m
//  InstaFix
//
//  Created by Destiny Bonavita on 5/4/15.
//  Copyright (c) 2015 Young and Strong Productions. All rights reserved.
//

#import "UITextField+Editing.h"
#import <objc/runtime.h>

static const void *LastFrameKey = &LastFrameKey;

@implementation UITextField (Editing)

#pragma mark - Functions

- (BOOL)becomeFirstResponder:(UILabel *)label
{
    self.lastFrame = label.frame;
    [UIView animateWithDuration:0.25 animations:^{
        CGRect frame = self.lastFrame;
        frame.origin = CGPointZero;
        
        label.frame = frame;
    }];
    
    return [super becomeFirstResponder];
}

- (BOOL)resignFirstResponder:(UILabel *)label
{
    [UIView animateWithDuration:0.25 animations:^{
        label.frame = self.lastFrame;
    }];
    
    return [super resignFirstResponder];
}

#pragma mark - Properties

- (void)setLastFrame:(CGRect)lastFrame
{
    objc_setAssociatedObject(self, LastFrameKey, [NSValue valueWithCGRect:lastFrame], OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (CGRect)lastFrame
{
    NSValue *val = objc_getAssociatedObject(self, LastFrameKey);
    return [val CGRectValue];
}

@end
