//
//  RGBCell.m
//  InstaFix
//
//  Created by Destiny Bonavita on 5/3/15.
//  Copyright (c) 2015 Young and Strong Productions. All rights reserved.
//

#import "RGBCell.h"
#import "Settings.h"
#import "UIImageEditor.h"
#import "ColorSelectorViewController.h"

#import <QuartzCore/QuartzCore.h>

@implementation RGBCell {
    RGBCellChangedBlock _delegate;
}

#pragma mark - Public

- (id)initWithFrame:(CGRect)frame
{
    RGBCell *view = [[[NSBundle mainBundle] loadNibNamed:@"RGBCell" owner:self options:0] firstObject];
    if (view)
    {
        view.frame = frame;
        
        //Styling
        view.colorView.layer.borderWidth = 1.0f;
        view.colorView.userInteractionEnabled = YES;
        view.colorView.layer.borderColor = [UIColor blackColor].CGColor;
        
        //Creates the color tap to press gesture
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:view action:@selector(tappedGesture:)];
        view.toggleLabel.text = @"Press\nMe";
        view.toggleLabel.userInteractionEnabled = YES;
        
        //Adds it
        [view.toggleLabel addGestureRecognizer:tap];
        
        //Adds the add to favorites gesture
        UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:view action:@selector(saveColor:)];
        [view.toggleLabel addGestureRecognizer:longPress];
        
        //Sets the sliders and the styling
        [view sliderValueChanged:nil];
    }
    
    return view;
}

#pragma mark - Properties

- (void)setSuperController:(ColorSelectorViewController *)superController
{
    if (_superController != superController) {
        _superController = superController;
        
        const CGFloat *colors = CGColorGetComponents([ImageHandler instance].imageView.backgroundColor.CGColor);
        self.redSlider.value = colors[0];
        self.blueSlider.value = colors[2];
        self.greenSlider.value = colors[1];
    }
}

- (void)setDelegate:(RGBCellChangedBlock)delegate
{
    _delegate = delegate;
    [self sliderValueChanged:nil];
}

#pragma mark - IBActions

- (IBAction)sliderValueChanged:(id)sender
{
    CGFloat red = self.redSlider.value;
    CGFloat blue = self.blueSlider.value;
    CGFloat green = self.greenSlider.value;
    
    UIColor *color = [UIColor colorWithRed:red green:green blue:blue alpha:1.0f];
    self.colorView.backgroundColor = color;
    
    if (_delegate)
        _delegate(red, green, blue, 1.0f);
}

#pragma mark - UIGestures

- (void)tappedGesture:(UITapGestureRecognizer *)gesture
{
    if (![Settings instance].ignoreTapToColorTutorial)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"InstaFix" message:@"Create a color from tapping anywhere on the screen." preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            [self addGlobalGestureRecognizer];
        }]];
        [alert addAction:[UIAlertAction actionWithTitle:@"Don't show again" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [Settings instance].ignoreTapToColorTutorial = YES;
            [self addGlobalGestureRecognizer];
        }]];
        
        [self.superController.view.window.rootViewController presentViewController:alert animated:YES completion:nil];
    }
    else
    {
        //Adds the gesture recognizer
        [self addGlobalGestureRecognizer];
    }
}

- (void)saveColor:(UILongPressGestureRecognizer *)longPress
{
    if (longPress.state == UIGestureRecognizerStateBegan)
    {
        ColorSelectorViewController *s = (ColorSelectorViewController *)self.superController;
        NSMutableArray *colors = s.favoriteBackgrounds;
        
        if (![colors containsObject:self.colorView.backgroundColor]) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"InstaFix" message:@"What would you like to do with this color?" preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
            [alert addAction:[UIAlertAction actionWithTitle:@"Add to Favorites" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                [colors addObject:self.colorView.backgroundColor];
                [s setFavoriteBackgrounds:colors];
            }]];
            
            [s.view.window.rootViewController presentViewController:alert animated:YES completion:nil];
        }
        else {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"InstaFix" message:@"What would you like to do with this color?" preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
            [alert addAction:[UIAlertAction actionWithTitle:@"Remove from Favorites" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                [colors removeObject:self.colorView.backgroundColor];
                [s setFavoriteBackgrounds:colors];
            }]];
            
            [s.view.window.rootViewController presentViewController:alert animated:YES completion:nil];
        }
    }
}

#pragma mark - Private

- (void)addGlobalGestureRecognizer
{
    //Sets label
    self.toggleLabel.text = @"Tap\nPicture";
    
    //Adds Gesture Recognizer
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapColor:)];
    tap.cancelsTouchesInView = YES;
    
    [ImageHandler instance].imageView.ImageEditorTapToColor = ^(UITapGestureRecognizer *tap) {
        [self tapColor:tap];
    };
}

- (void)tapColor:(UITapGestureRecognizer *)tap
{
    //Gets the color
    UIColor *color = [tap.view colorOfPoint:[tap locationInView:tap.view]];
    
    //Sets label
    self.toggleLabel.text = @"Press\nMe";
    
    //Sets the sliders
    const CGFloat *colors = CGColorGetComponents(color.CGColor);
    self.redSlider.value = colors[0];
    self.blueSlider.value = colors[2];
    self.greenSlider.value = colors[1];
    
    //Sets all color placements
    [self sliderValueChanged:nil];
}

@end
