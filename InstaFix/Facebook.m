//
//  Facebook.m
//  InstaFix
//
//  Created by Destiny Dawn on 12/27/14.
//  Copyright (c) 2014 Young and Strong Productions. All rights reserved.
//

#import "Facebook.h"

@implementation Facebook

#pragma mark - Class Managment

+ (instancetype)instance
{
    static Facebook *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[Facebook alloc] init];
    });
    
    return instance;
}

#pragma mark - Public

- (BOOL)isSignedIn
{
    if (FBSession.activeSession.state == FBSessionStateOpen || FBSession.activeSession.state == FBSessionStateOpenTokenExtended)
    {
        return [FBSession.activeSession.permissions containsObject:@"user_photos"];
    }
    else
        return NO;
}

- (void)signIn:(FBSessionStateHandler)handler
{
    [FBSession openActiveSessionWithReadPermissions:@[@"user_photos"] allowLoginUI:YES completionHandler:handler];
}

@end
