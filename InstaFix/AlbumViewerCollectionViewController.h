//
//  AlbumViewerCollectionViewController.h
//  InstaFix
//
//  Created by Destiny Dawn on 12/27/14.
//  Copyright (c) 2014 Young and Strong Productions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhotoSelectorDelegate.h"

@interface AlbumViewerCollectionViewController : UICollectionViewController

@property (nonatomic, retain) NSURL *groupURL;
@property (nonatomic, retain) id<PhotoSelectorDelegate> delegate;

@end
