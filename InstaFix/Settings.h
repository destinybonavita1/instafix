//
//  Settings.h
//  InstaFix
//
//  Created by Destiny Bonavita on 5/3/15.
//  Copyright (c) 2015 Young and Strong Productions. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    TutorialPictureEditing = 0,
    TutorialAddingPicture = 1,
    TutorialAutoFormating = 2,
    TutorialCaption = 3,
    TutorialBackgroundColoring = 4,
    TutorialImageSharing = 5
} TutorialStatus;

@interface Settings : NSObject

+ (instancetype)instance;

//Photo Formats
@property (nonatomic, readonly) NSDictionary *formats;
@property (nonatomic, readwrite) BOOL autoFormat;

//Tutorials
@property (nonatomic, readwrite) TutorialStatus tutorialStatus;
@property (nonatomic, readwrite) NSDictionary *tutorialSwipeClose;
@property (nonatomic, readwrite) BOOL tutorialColorGrabbing;
@property (nonatomic, readwrite) BOOL tutorialBasicColoring;
@property (nonatomic, readwrite) BOOL tutorialGrabAll;

//Social Networking
@property (nonatomic, readwrite) BOOL autoLoad;
@property (nonatomic, readwrite) BOOL ignoreTapToColorTutorial;

@end
