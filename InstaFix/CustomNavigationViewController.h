//
//  CustomNavigationViewController.h
//  InstaFix
//
//  Created by Destiny Dawn on 1/15/14.
//  Copyright (c) 2014 Young and Strong Productions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomNavigationViewController : UINavigationController

@end
