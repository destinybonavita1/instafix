//
//  ImageHandler.h
//  InstaFix Pro
//
//  Created by Destiny Dawn on 8/5/13.
//  Copyright (c) 2013 Young and Strong Productions. All rights reserved.
//

#import "UIImageEditor.h"

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <Foundation/Foundation.h>

@interface ImageHandler : NSObject <UIGestureRecognizerDelegate>

+ (instancetype)instance;
+ (NSString *)saveImage:(UIImage *)image;
+ (UIImage *)resizeImage:(UIImage *)image;
+ (UIImage *)resizeImage:(UIImage *)image color:(UIColor *)color;

@property (nonatomic, retain) UIImageEditor *imageView;
@property (nonatomic, readonly) NSString *imageDirectory;

- (UIImage *)finalizeImage;
- (UIImage *)finalizedImage;
- (CGSize)sizeToSquare:(CGSize)size;

@end
