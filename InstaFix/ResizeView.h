//
//  ResizeView.h
//  InstaFix
//
//  Created by Destiny Bonavita on 5/6/15.
//  Copyright (c) 2015 Young and Strong Productions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResizeView : UIView

@property (nonatomic, retain) NSArray *json;

@end
