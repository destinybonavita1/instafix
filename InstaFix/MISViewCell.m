//
//  MISViewCell.m
//  InstaFix
//
//  Created by Destiny Dawn on 11/20/13.
//  Copyright (c) 2013 Young and Strong Productions. All rights reserved.
//

#import "MISViewCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation MISViewCell
@synthesize imageDisplay;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        imageDisplay = [[UIImageView alloc] initWithFrame:self.contentView.bounds];
        [self.contentView addSubview:imageDisplay];
        
        self.loadMoreLbl = [[UILabel alloc] initWithFrame:self.contentView.bounds];
        self.loadMoreLbl.text = @"Load\nMore";
        self.loadMoreLbl.numberOfLines = 2;
        self.loadMoreLbl.textAlignment = NSTextAlignmentCenter;
        self.isLoadMore = NO;
        
        [self.contentView addSubview:self.loadMoreLbl];
    }
    return self;
}

- (void)setIsSelected:(BOOL)isSelected
{
    self.layer.borderWidth = (isSelected) ? 1.0f : 0.0f;
    self.layer.borderColor = [UIColor blueColor].CGColor;
}

- (void)setIsLoadMore:(BOOL)isLoadMore
{
    if (_isLoadMore != isLoadMore)
        _isLoadMore = isLoadMore;
    self.loadMoreLbl.hidden = !isLoadMore;
    self.imageDisplay.hidden = isLoadMore;
}

- (void)setPhoto:(UIImage *)photo {
    self.imageDisplay.image = photo;
    self.imageDisplay.contentMode = UIViewContentModeScaleAspectFit;
}

@end
