//
//  UIView+ColorView.h
//  InstaFix
//
//  Created by Destiny Bonavita on 4/27/15.
//  Copyright (c) 2015 Young and Strong Productions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (ColorView)

- (UIColor *)colorOfPoint:(CGPoint)point;   

@end
