//
//  Facebook.h
//  InstaFix
//
//  Created by Destiny Dawn on 12/27/14.
//  Copyright (c) 2014 Young and Strong Productions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FacebookSDK/FacebookSDK.h>

@interface Facebook : NSObject

@property (nonatomic, retain) NSMutableArray *photoUrls;
@property (nonatomic, retain) NSMutableArray *photos;

- (BOOL)isSignedIn;
- (void)signIn:(FBSessionStateHandler)handler;

+ (instancetype)instance;

@end
