//
//  ColorCell.h
//  InstaFix
//
//  Created by Destiny Bonavita on 4/27/15.
//  Copyright (c) 2015 Young and Strong Productions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ColorCell : UIView

@property (nonatomic, retain) UIViewController *superController;

+ (id)colorCellWithFrame:(CGRect)frame color:(UIColor *)color;
+ (id)colorCellWithFrame:(CGRect)frame R:(CGFloat)R G:(CGFloat)G B:(CGFloat)B;

@property (nonatomic) BOOL isNoFavorites;

@end
