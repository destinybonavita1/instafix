//
//  Settings.m
//  InstaFix
//
//  Created by Destiny Bonavita on 5/3/15.
//  Copyright (c) 2015 Young and Strong Productions. All rights reserved.
//

#import "Settings.h"

@interface Settings ()

@property (nonatomic, readonly) NSDictionary *localFormats;

@end

@implementation Settings

#pragma mark - Public

+ (instancetype)instance
{
    static Settings *s;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        s = [[Settings alloc] init];
        
        //Downloads the latest formats
        [s downloadFormats];
    });
    
    return s;
}

#pragma mark - Photo Formats

- (NSDictionary *)formats
{
    if (!self.localFormats)
        return [self downloadFormats];
    else
        return self.localFormats;
}

- (NSDictionary *)localFormats
{
    NSData *data = [NSData dataWithContentsOfFile:kFormatsPath];
    return [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
}

- (NSDictionary *)downloadFormats
{
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://trensder.com/formats.json"] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:1];
    
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    if (data) {
        [data writeToFile:kFormatsPath atomically:NO];
        
        return [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    }
    else
        return nil;
}

#pragma mark - Photo Formatting Settings

- (BOOL)autoFormat
{
    if (![UserDefaults objectForKey:kShouldFormat])
        return YES;
    
    return [UserDefaults boolForKey:kShouldFormat];
}

- (void)setAutoFormat:(BOOL)autoFormat
{
    [UserDefaults setBool:autoFormat forKey:kShouldFormat];
    [UserDefaults synchronize];
}

#pragma mark - Tutorial Status

- (TutorialStatus)tutorialStatus
{
    return [UserDefaults integerForKey:kTutorialStatus];
}

- (void)setTutorialStatus:(TutorialStatus)tutorialStatus
{
    [UserDefaults setInteger:tutorialStatus forKey:kTutorialStatus];
    [UserDefaults synchronize];
}

- (NSDictionary *)tutorialSwipeClose
{
    NSDictionary *dict = [UserDefaults valueForKey:kTutorialSwipe];
    if (dict)
        return [UserDefaults valueForKey:kTutorialSwipe];
    else
    {
        self.tutorialSwipeClose = [NSDictionary new];
        return self.tutorialSwipeClose;
    }
}

- (void)setTutorialSwipeClose:(NSDictionary *)tutorialSwipeClose
{
    [UserDefaults setValue:tutorialSwipeClose forKey:kTutorialSwipe];
    [UserDefaults synchronize];
}

- (BOOL)tutorialBasicColoring
{
    return [UserDefaults boolForKey:kBasicColoring];
}

- (void)setTutorialBasicColoring:(BOOL)tutorialBasicColoring
{
    [UserDefaults setBool:tutorialBasicColoring forKey:kBasicColoring];
    [UserDefaults synchronize];
}

- (BOOL)tutorialColorGrabbing
{
    return [UserDefaults boolForKey:kColorGrabbing];
}

- (void)setTutorialColorGrabbing:(BOOL)tutorialColorGrabbing
{
    [UserDefaults setBool:tutorialColorGrabbing forKey:kColorGrabbing];
    [UserDefaults synchronize];
}

- (BOOL)tutorialGrabAll
{
    return [UserDefaults boolForKey:kAutoLoadAllTutorial];
}

- (void)setTutorialGrabAll:(BOOL)tutorialGrabAll
{
    [UserDefaults setBool:tutorialGrabAll forKey:kAutoLoadAllTutorial];
    [UserDefaults synchronize];
}

#pragma mark - Social Networking Settings

- (BOOL)autoLoad
{
    return [UserDefaults boolForKey:kAutoLoadAll];
}

- (void)setAutoLoad:(BOOL)autoLoad
{
    [UserDefaults setBool:autoLoad forKey:kAutoLoadAll];
    [UserDefaults synchronize];
}

#pragma mark - Color Changing Settings

- (BOOL)ignoreTapToColorTutorial
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"tapToColor"];
}

- (void)setIgnoreTapToColorTutorial:(BOOL)ignoreTapToColorTutorial
{
    [[NSUserDefaults standardUserDefaults] setBool:ignoreTapToColorTutorial forKey:@"tapToColor"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
