//
//  PhotoEditorViewController.m
//  InstaFix
//
//  Created by Destiny Dawn on 11/17/13.
//  Copyright (c) 2013 Young and Strong Productions. All rights reserved.
//

#import "Settings.h"
#import "AppDelegate.h"
#import "UIImageEditor.h"
#import "ResizingViewController.h"
#import "UIImageView+GestureImage.h"
#import "PhotoEditorViewController.h"
#import "PhotoSelectorTableViewController.h"

@interface PhotoEditorViewController () <UITextFieldDelegate, ResizingViewControllerDelegate>

@property (nonatomic, weak) IBOutlet UIBarButtonItem *autoButton;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *captionButton;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *addPictureButton;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *shareImageButton;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *colorBackgroundButton;

@property (nonatomic, retain) UILabel *captionLabel;
@property (nonatomic, weak) IBOutlet UITextField *tempField;
@property (nonatomic, retain) GestureImage *lastSelectedGesture;
@property (nonatomic, retain) ResizingViewController *resizingSelector;

@end

@implementation PhotoEditorViewController
@synthesize aspectRatioSelector, imageDisplay, imageCount, bottomToolBar;

#pragma mark - UIView Life Cycle

- (void)viewDidLoad
{
    //Calls the super viewDidLoad
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    self.imageCount = 0;
    
    //Adds a temperary field to help edit the caption
    self.tempField.delegate = self;
    
    [ImageHandler instance].imageView = self.imageDisplay;
    
    //Adds notification listeners
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tutorialPopoverFinished:) name:kTutorialPopoverDimissed object:nil];
    
    //Designs the view
    [self designView];
}

- (void)viewDidAppear:(BOOL)animated
{
    if (imageCount == 0) [self addImage:nil];
    
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBActions

- (IBAction)addText:(id)sender {
    if (!self.captionLabel) {
        CGRect frame = CGRectZero;
        frame.size.width = CGRectGetWidth(self.imageDisplay.frame);
        frame.size.height = CGRectGetHeight(self.imageDisplay.frame) * 0.15f;
        frame.origin.y = CGRectGetHeight(self.imageDisplay.frame) - frame.size.height;
        
        self.captionLabel = [[UILabel alloc] initWithFrame:frame];
        self.captionLabel.font = [UIFont boldSystemFontOfSize:(frame.size.height - (frame.size.height * 0.15f))];
        self.captionLabel.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
        self.captionLabel.textAlignment = NSTextAlignmentCenter;
        self.captionLabel.adjustsFontSizeToFitWidth = YES;
        self.captionLabel.userInteractionEnabled = YES;
        self.captionLabel.textColor = [UIColor whiteColor];
        self.captionLabel.backgroundColor = [UIColor colorWithWhite:0.5 alpha:0.25];
        
        //Creates the gestures
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(textFieldShouldBegin)];
        UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanLbl:)];
        UILongPressGestureRecognizer *touchAndHoldRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(touchAndHold:)];
        
        //Sets gesture properties
        touchAndHoldRecognizer.minimumPressDuration = 0.3;
        
        //Adds the gestures
        [self.captionLabel addGestureRecognizer:tap];
        [self.captionLabel addGestureRecognizer:pan];
        [self.captionLabel addGestureRecognizer:touchAndHoldRecognizer];
        
        //Adds the label
        [imageDisplay addSubview:self.captionLabel];
    }
    else
        self.captionLabel.alpha = 1.0f;
    
    [self.tempField becomeFirstResponder:self.captionLabel];
}

- (IBAction)addImage:(id)sender {
    UINavigationController *nav = [[UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil] instantiateViewControllerWithIdentifier:@"selectPhotoMode"];
    [nav popToRootViewControllerAnimated:NO];
    
    [self presentViewController:nav animated:YES completion:^{
        PhotoSelectorTableViewController *selector = (PhotoSelectorTableViewController *)nav.topViewController;
        [selector view];
        
        if (imageCount > 0)
            selector.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:selector action:@selector(cancelPhotoSelector)];
        selector.delegate = self;
    }];
}

- (IBAction)shareImage:(id)sender {
#warning Replace the UIActionController
    [[[UIActionSheet alloc] initWithTitle:@"Where would you like to share this image?" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Instagram", @"Twitter", @"Save Photo", nil] showFromTabBar:[[[self navigationController] tabBarController] tabBar]];
}

- (IBAction)selectBackground:(id)sender {
    [self.colorSelector showInView:self.view aboveView:self.bottomToolBar];
}

- (IBAction)organizeSubviews:(id)sender {
    NSDictionary *dict = [Settings instance].formats;
    NSArray *json = dict[[NSString stringWithFormat:@"%@", @(imageCount)]];
    if (json) {
        if (json.count > 1)
        {
            self.resizingSelector.json = json;
            
            [self.resizingSelector showInView:self.view aboveView:self.bottomToolBar];
        }
        else
            [self organizeWithJSON:json[0]];
    }
    else if (!dict || !json)
        [self organizeSubviews];
}

- (IBAction)changeSettings:(id)sender {
    [[[UIActionSheet alloc] initWithTitle:@"Photo Settings" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Change Frame Size", @"Change Background Image", nil] showFromToolbar:bottomToolBar];
}

#pragma mark - Private

- (void)designView {
    //Sets the background and border color of the UIScrollView
    imageDisplay.layer.borderWidth = 2.0f;
    imageDisplay.layer.borderColor = [UIColor blackColor].CGColor;
    
    //Sets the views
    self.colorSelector = [[AppDelegate storyboard] instantiateViewControllerWithIdentifier:@"ColorSelector"];
    self.aspectRatioSelector = [[AppDelegate storyboard] instantiateViewControllerWithIdentifier:@"AspectRatioSelector"];
    self.resizingSelector = [[AppDelegate storyboard] instantiateViewControllerWithIdentifier:@"resizingViewController"];
    
    //Sets the delegates
    self.colorSelector.delegate = self;
    self.resizingSelector.delegate = self;
    self.aspectRatioSelector.delegate = self;
    
    //Sets the frames
    self.colorSelector.view.frame = CGRectMake(0, [[UIScreen mainScreen] bounds].size.height + 200, 320, 200);
    self.resizingSelector.view.frame = CGRectMake(0, [[UIScreen mainScreen] bounds].size.height + 200, 320, 190);
    self.aspectRatioSelector.view.frame = CGRectMake(0, [[UIScreen mainScreen] bounds].size.height + 200, 320, 190);
    
    //Adds the view controllers
    [self addChildViewController:self.colorSelector];
    [self addChildViewController:self.resizingSelector];
    [self addChildViewController:self.aspectRatioSelector];
}

- (void)organizeSubviews
{
    CGFloat x = 0.0f;
    CGFloat y = 0.0f;
    CGRect bounds = imageDisplay.frame;
    CGFloat mtlpl = (CGFloat)imageCount / 4.0;
    int mtlplc = (int)mtlpl;
    BOOL t = (mtlpl == (CGFloat)mtlplc);
    
    for (UIView *imageView in [imageDisplay subviews]) {
        if ([imageView isKindOfClass:[GestureImage class]]) {
            GestureImage *i = (GestureImage *)imageView;
            
            if (![i PositionLock]) {
                CGRect frame = i.boundingFrame;
                
                frame.size.width = bounds.size.width / [self customRounding:((t) ? imageCount - 1 : imageCount)];
                frame.size.height = frame.size.width;
                
                if (imageCount == 1) {
                    frame.origin.x = x;
                    frame.origin.y = (bounds.size.height * 0.5) - (frame.size.height * 0.5);
                } else if ((y + frame.size.height) <= bounds.size.height) {
                    frame.origin.x = x;
                    frame.origin.y = y;
                    y += frame.size.height;
                } else {
                    y = 0.0f;
                    x += frame.size.width;
                    
                    frame.origin.x = x;
                    frame.origin.y = y;
                    y += frame.size.height;
                }
                
                [imageView setFrame:frame];
            }
        } else if ([imageView isKindOfClass:[UILabel class]]) {
            UILabel *newLbl = (UILabel *)imageView;
            CGRect frame = newLbl.frame;
            frame.size.width = bounds.size.width;
            frame.origin.x   = 0;
            
            [newLbl setFrame:frame];
        }
    }
}

- (void)organizeWithJSON:(NSArray *)json
{
    if (imageCount == json.count) {
        int index = 0;
        CGFloat width = CGRectGetWidth(imageDisplay.frame);
        CGFloat height = CGRectGetHeight(imageDisplay.frame);
        
        for (GestureImage *img in imageDisplay.subviews) {
            if ([img isKindOfClass:[GestureImage class]]) {
                NSDictionary *j = json[index];
                CGRect frame = img.frame;
                frame.origin.x = width * [j[@"left"] floatValue];
                frame.origin.y = height * [j[@"top"] floatValue];
                frame.size.width = width * [j[@"width"] floatValue];
                frame.size.height = height * [j[@"height"] floatValue];
                
                img.frame = frame;
                
                index++;
            }
            else
                continue;
        }
    }
}

- (float)customRounding:(float)value {
    for (int i = 0; i < (int)MAXFLOAT; i++) {
        if (value <= (i * i))
            return i;
    }
    
    return 0;
}

- (BOOL)canBecomeFirstResponder {
    return YES;
}

#pragma mark - UIGestureRecognizers

- (void)handlePinch:(UIPinchGestureRecognizer *)sender {
    sender.view.transform = CGAffineTransformScale(sender.view.transform, sender.scale, sender.scale);
    sender.scale = 1;
}

- (void)handleRotate:(UIRotationGestureRecognizer *)sender {
    sender.view.transform = CGAffineTransformRotate(sender.view.transform, sender.rotation);
    sender.rotation = 0;
}

- (void)handlePan:(UIPanGestureRecognizer *)sender {
    CGPoint translation = [sender translationInView:imageDisplay];
    if ([[sender view] isKindOfClass:[GestureImage class]]) {
        GestureImage *view = (GestureImage *)sender.view;
        if (![view PositionLock]) {
            sender.view.center = CGPointMake(sender.view.center.x + translation.x,
                                             sender.view.center.y + translation.y);
            [sender setTranslation:CGPointMake(0, 0) inView:imageDisplay];
        }
        
        return;
    }
    
    sender.view.center = CGPointMake(sender.view.center.x + translation.x,
                                     sender.view.center.y + translation.y);
    [sender setTranslation:CGPointMake(0, 0) inView:imageDisplay];
}

- (void)handlePanLbl:(UIPanGestureRecognizer *)sender {
    CGPoint translation = [sender translationInView:imageDisplay];
    CGFloat minY = 0;
    CGFloat maxY = CGRectGetHeight(imageDisplay.frame) - CGRectGetHeight(sender.view.frame);
    CGFloat newY = CGRectGetMinY(sender.view.frame) + translation.y;
    
    if (newY > minY && newY < maxY)
    {
        sender.view.center = CGPointMake(sender.view.center.x, sender.view.center.y + translation.y);
        [sender setTranslation:CGPointMake(0, 0) inView:imageDisplay];
    }
}

- (void)handleSingleTap:(UITapGestureRecognizer *)sender {
    if (self.lastSelectedGesture) {
        CGRect last = sender.view.frame;
        
        sender.view.frame = self.lastSelectedGesture.frame;
        self.lastSelectedGesture.frame = last;
    }
    else if (self.imageDisplay.ImageEditorTapToColor) {
        self.imageDisplay.ImageEditorTapToColor(sender);
        self.imageDisplay.ImageEditorTapToColor = nil;
    }
    
    self.lastSelectedGesture = nil;
}

- (void)handleDoubleTap:(UITapGestureRecognizer *)sender
{
    sender.view.transform = CGAffineTransformRotate(sender.view.transform, DegreesToRadians(90.0f));
}

- (void)touchAndHold:(UILongPressGestureRecognizer *)sender {
    if ([sender state] == UIGestureRecognizerStateRecognized) {
        obj = sender.view;
        
        //Gets the framing
        UIView *view = sender.view;
        CGRect frame = view.frame;
        frame.origin.x = frame.origin.x + (frame.size.width / 2);
        frame.origin.y = frame.origin.y + frame.size.height;
        
        //Requests first responder
        [self becomeFirstResponder];
        
        //Creates the menu
        UIMenuController *menu = [UIMenuController sharedMenuController];
        
        //UIMenuController Items
        UIMenuItem *deleteBtn = [[UIMenuItem alloc] initWithTitle:@"Remove" action:@selector(removeImg:)];
        UIMenuItem *swapPosition = [[UIMenuItem alloc] initWithTitle:@"Swap" action:@selector(swapImage:)];
        UIMenuItem *horizontalLockBtn = [[UIMenuItem alloc] initWithTitle:@"Lock" action:@selector(horizontalLock:)];
        UIMenuItem *bringToFrontBtn = [[UIMenuItem alloc] initWithTitle:@"Bring To Front" action:@selector(bringToFront:)];
        UIMenuItem *sendToBackBtn = [[UIMenuItem alloc] initWithTitle:@"Send To Back" action:@selector(sendToBack:)];
        
        UIMenuItem *changeColor = [[UIMenuItem alloc] initWithTitle:@"Change Color" action:@selector(changeColor:)];
        UIMenuItem *editBtn = [[UIMenuItem alloc] initWithTitle:@"Edit" action:@selector(editLbl:)];
        
        //Sets the menu items
        if ([view isKindOfClass:[GestureImage class]])
            menu.menuItems = @[swapPosition, horizontalLockBtn, bringToFrontBtn, sendToBackBtn, deleteBtn];
        else if ([view isKindOfClass:[UILabel class]])
            menu.menuItems = @[editBtn, changeColor, bringToFrontBtn, sendToBackBtn, deleteBtn];
        
        //Shows the menu
        [menu setTargetRect:frame inView:[self imageDisplay]];
        [menu setMenuVisible:YES animated:YES];
    }
}

#pragma mark - UIMenuController Items

- (void)horizontalLock:(UIMenuController *)sender {
    if ([obj isKindOfClass:[GestureImage class]]) {
        GestureImage *view = (GestureImage *)obj;
        view.PositionLock = !view.PositionLock;
    }
}

- (void)bringToFront:(UIMenuController *)sender {
    if (obj != nil) {
        if ([obj isKindOfClass:[UILabel class]]) {
            [(UILabel *)obj bringSubviewToFront:imageDisplay];
        } else if ([obj isKindOfClass:[GestureImage class]]) {
            [imageDisplay insertSubview:(GestureImage *)obj aboveSubview:imageDisplay];
        }
    }
    
    obj = nil;
}

- (void)sendToBack:(UIMenuController *)sender {
    if (obj != nil) {
        if ([obj isKindOfClass:[UILabel class]]) {
            [(UILabel *)obj sendSubviewToBack:imageDisplay];
        } else if ([obj isKindOfClass:[GestureImage class]]) {
            [imageDisplay insertSubview:(GestureImage *)obj atIndex:0];
        }
    }
    
    obj = nil;
}

- (void)removeImg:(UIMenuController *)sender {
    if (obj) {
        if ([obj isKindOfClass:[GestureImage class]]) {
            [((GestureImage *)obj) removeFromSuperview];
            
            imageCount--;
        } else if ([obj isKindOfClass:[UILabel class]]) {
            //Removes the caption label
            [self.captionLabel removeFromSuperview];
            
            //Dealicates it
            self.captionLabel = nil;
        }
    }
    
    obj = nil;
}

- (void)changeColor:(UIMenuController *)sender {
    isColor = YES;
    
    UIView *v = [self.colorSelector view];
    [v setFrame:CGRectMake(0, [[UIScreen mainScreen] bounds].size.height + 200, 320, 190)];
    [v setAlpha:0.0];
    [[self view] insertSubview:v aboveSubview:imageDisplay];
    
    if ([[[UIDevice currentDevice] model] hasPrefix:@"iPad"])
        [v setAutoresizingMask:UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleWidth];
    
    [UIView animateWithDuration:0.5 animations:^(void) {
        CGRect frame = bottomToolBar.frame;
        frame.size.height = 130;
        frame.origin.y -= frame.size.height;
        
        [v setFrame:frame];
        [v setAlpha:1.0];
        [self.colorSelector setColorViewChoices];
    }];
}

- (void)swapImage:(UIMenuController *)sender
{
    self.lastSelectedGesture = (GestureImage *)obj;
}

- (void)editLbl:(UIMenuController *)sender {
    [self.tempField becomeFirstResponder:self.captionLabel];
}

#pragma mark - NSNotification Listeners

- (void)tutorialPopoverFinished:(NSNotification *)notification
{
    Settings *settings = [Settings instance];
    if (settings.tutorialStatus == TutorialPictureEditing) {
        settings.tutorialStatus = TutorialAddingPicture;
        [self displayTutorialWithMessage:@"Press this to add pictures to your collage." aboveView:[self.addPictureButton valueForKey:@"view"]];
    }
    else if (settings.tutorialStatus == TutorialAddingPicture) {
        settings.tutorialStatus = TutorialAutoFormating;
        [self displayTutorialWithMessage:@"Press this to automatically position all your pictures in a grid." aboveView:[self.autoButton valueForKey:@"view"]];
    }
    else if (settings.tutorialStatus == TutorialAutoFormating) {
        settings.tutorialStatus = TutorialCaption;
        [self displayTutorialWithMessage:@"Press this to add a caption to your photo." aboveView:[self.captionButton valueForKey:@"view"]];
    }
    else if (settings.tutorialStatus == TutorialCaption) {
        settings.tutorialStatus = TutorialBackgroundColoring;
        [self displayTutorialWithMessage:@"Press this to change your background color." aboveView:[self.colorBackgroundButton valueForKey:@"view"]];
    }
    else if (settings.tutorialStatus == TutorialBackgroundColoring) {
        settings.tutorialStatus = TutorialImageSharing;
        [self displayTutorialWithMessage:@"Press this to share or save your picture when you're done." aboveView:[self.shareImageButton valueForKey:@"view"]];
    }
    
    //Tutorial Finished
//    else if (settings.tutorialStatus == TutorialImageSharing) {
//        
//    }
}

#pragma mark - UIActionSheet Delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if ([[actionSheet title] isEqualToString:@"Where would you like to share this image?"]) {
        NSString *title = [actionSheet buttonTitleAtIndex:buttonIndex];
        if ([title isEqualToString:@"Facebook"]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Photo Upload" message:@"What do you want to title this as?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Upload", nil];
            [alert setAlertViewStyle:UIAlertViewStylePlainTextInput];
            [alert show];
        } else if ([title isEqualToString:@"Twitter"]) {
            if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
                SLComposeViewController *twitterSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
                [twitterSheet setInitialText:@"Image edited with #InstaFixApp for iOS."];
                [twitterSheet addImage:[[ImageHandler instance] finalizeImage]];
                [self presentViewController:twitterSheet animated:YES completion:nil];
            }  else {
                [[[UIAlertView alloc] initWithTitle:@"Error" message:@"You can't post this, you do not have a Twitter account setup with your iOS device." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
            }
        } else if ([title isEqualToString:@"Instagram"]) {
            NSURL *instagramURL = [NSURL URLWithString:@"instagram://app"];
            if ([[UIApplication sharedApplication] canOpenURL:instagramURL])
            {
                CGRect rect = CGRectMake(0 ,0 , 612, 612);
                NSString  *jpgPath = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/InstaFix_Normal.ig"];
                NSURL *igImageHookFile = [[NSURL alloc] initWithString:[[NSString alloc] initWithFormat:@"file://%@", jpgPath]];
                
                self.dic = [UIDocumentInteractionController interactionControllerWithURL:igImageHookFile];
                [self.dic setDelegate:self];
                [self storeimage:[[ImageHandler instance] finalizeImage]];
                [[self dic] setAnnotation:[NSDictionary dictionaryWithObjectsAndKeys:@"\n\nImage Edited via #InstaFixApp",@"InstagramCaption", nil]];
                [self.dic presentOpenInMenuFromRect:rect inView:self.view animated:YES];
            }
            else
            {
                [[[UIAlertView alloc] initWithTitle:@"Instagram unavailable " message:@"You need to install Instagram on your device in order to share this image" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            }
        } else if ([title isEqualToString:@"Save Photo"]) {
            UIImageWriteToSavedPhotosAlbum([[ImageHandler instance] finalizedImage], nil, nil, nil);
            
            [[[UIAlertView alloc] initWithTitle:@"Saved" message:@"Your photo has been saved!" delegate:nil cancelButtonTitle:@"Close" otherButtonTitles:nil, nil] show];
        }
    } else if ([[actionSheet title] isEqualToString:@"Photo Settings"]) {
        if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Change Frame Size"]) {
            UIView *v = [aspectRatioSelector view];
            [v setFrame:CGRectMake(0, [[UIScreen mainScreen] bounds].size.height + 200, 320, 190)];
            [v setAlpha:0.0];
            [[self view] insertSubview:v aboveSubview:imageDisplay];
            
            if ([[[UIDevice currentDevice] model] hasPrefix:@"iPad"])
                [v setAutoresizingMask:UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleWidth];
            
            [UIView animateWithDuration:0.5 animations:^(void) {
                CGRect frame = bottomToolBar.frame;
                frame.size.height = 130;
                frame.origin.y -= frame.size.height;
                
                [v setFrame:frame];
                [v setAlpha:1.0];
                [aspectRatioSelector SetAspectRatioOptions];
            }];
        } else if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Change Background Image"]) {
            isBackgroundImage = YES;
            
            [[[UIActionSheet alloc] initWithTitle:@"What would you like the background to add the image from?" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Camera", @"Gallery", nil] showFromTabBar:[[[self navigationController] tabBarController] tabBar]];
        }
    }
}

#pragma mark - Photo Selector Delegate

- (void)photoSelectorDelegate:(id)selector imageSelected:(UIImage *)image
{
    [self photoSelectorDelegate:selector imagesSelected:@[image]];
}

- (void)photoSelectorDelegate:(id)selector imagesSelected:(NSArray *)images
{
    UINavigationController *nav = [(UIViewController *)selector navigationController];
    if (!isBackgroundImage)
        imageCount += images.count;
    
    [nav dismissViewControllerAnimated:YES completion:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            if (isBackgroundImage) {
                imageDisplay.backgroundColor = [UIColor colorWithPatternImage:images.firstObject];
                isBackgroundImage = NO;
            }
            else
            {
                for (UIImage *image in images) {
                    UIImage *img = image;
                    GestureImage *view = [[GestureImage alloc] initWithImage:img];
                    view.multipleTouchEnabled = YES;
                    view.userInteractionEnabled = YES;
                    view.contentMode = UIViewContentModeScaleAspectFit;
                    view.frame = [self scaleSize:CGSizeMake(280, 280) orgSize:view.image.size];
                    
                    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
                    UITapGestureRecognizer *sap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
                    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
                    UIPinchGestureRecognizer *pinch = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinch:)];
                    UIRotationGestureRecognizer *rotate = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(handleRotate:)];
                    UILongPressGestureRecognizer *touchAndHoldRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(touchAndHold:)];
                    
                    //Sets recognizer properties
                    tap.numberOfTapsRequired = 2;
                    touchAndHoldRecognizer.minimumPressDuration = 0.3;
                    
                    //Adds all the gesture recognizers
                    [view addGestureRecognizer:tap];
                    [view addGestureRecognizer:sap];
                    [view addGestureRecognizer:pan];
                    [view addGestureRecognizer:pinch];
                    [view addGestureRecognizer:rotate];
                    [view addGestureRecognizer:touchAndHoldRecognizer];
                    
                    //Adds the image to the view
                    [imageDisplay addSubview:view];
                    
                    //Runs the tutorial if not editing
                    if ([Settings instance].tutorialStatus != TutorialPictureEditing)
                        [self tutorialPopoverFinished:nil];
                    else
                    {
                        [self displayTutorialWithMessage:@"Press and hold this picture to manage it, or double tap it to rotate it 90°." aboveView:view];
                    }
                    
                    //Automatically organize according to JSON
                    if ([Settings instance].autoFormat) {
                        NSDictionary *dict = [Settings instance].formats;
                        NSArray *json = dict[[NSString stringWithFormat:@"%@", @(imageCount)]];
                        if (json) {
                            [self organizeWithJSON:json[0]];
                        }
                        else if (!dict || !json)
                            [self organizeSubviews];
                    }
                }
            }
        });
    }];
}

- (CGRect)scaleSize:(CGSize)oldSize orgSize:(CGSize)orgSize {
    float factor = 1.0;
    CGRect frame = CGRectMake(0, 0, 0, 0);
    CGSize scaledSize = oldSize;
    if (orgSize.width > orgSize.height) {
        factor = orgSize.width / orgSize.height;
        scaledSize.width = oldSize.width;
        scaledSize.height = oldSize.height / factor;
    } else {
        factor = orgSize.height / orgSize.width;
        scaledSize.height = oldSize.height;
        scaledSize.width = oldSize.width / factor;
    }
    
    frame.size = scaledSize;
    return frame;
}

- (void)colorChange:(UIColor *)color {
    if (isColor) {
        self.captionLabel.textColor = color;
        return;
    }
    
    imageDisplay.backgroundColor = color;
}

#pragma mark - UITextField Delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    self.captionLabel.text = textField.text;
    return YES;
}

- (IBAction)textFieldDidChangeValue:(id)sender
{
    self.captionLabel.text = self.tempField.text;
}

- (void)textFieldShouldBegin
{
    [self.tempField becomeFirstResponder:self.captionLabel];
}

- (IBAction)textFieldShouldResign:(id)sender
{
    [self.tempField resignFirstResponder:self.captionLabel];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.tempField resignFirstResponder:self.captionLabel];
    return NO;
}

#pragma mark - AspectRatioController Delegate

- (void)aspectRatioController:(UIViewController *)aspectRatioController ratio:(float)ratio {
    CGRect frame = imageDisplay.frame;
    
    float newWidth;
    if ((int)(ratio * 100) == (int)7140 || (int)(ratio * 100) == (int)4071)
        newWidth   = roundf((frame.size.height / (int)ratio) * (int)floor((ratio - floor(ratio)) * 100));
    else
        newWidth   = roundf((frame.size.height / (int)ratio) * (int)floor((ratio - floor(ratio)) * 10));
    
    frame.size.width = newWidth;
    frame.origin.x  += newWidth * 0.5;
    
    [imageDisplay removeConstraints:[imageDisplay constraints]];
    [imageDisplay addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:[imageDisplay(==%f)]", frame.size.width] options:0 metrics:nil views:NSDictionaryOfVariableBindings(imageDisplay)]];
    [imageDisplay addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:[imageDisplay(==%f)]", frame.size.height] options:0 metrics:nil views:NSDictionaryOfVariableBindings(imageDisplay)]];
    
    [imageDisplay setFrame:frame];
    
    if ([Settings instance].autoFormat)
        [self organizeSubviews];
}

#pragma mark - ResizingViewController Delegate

- (void)resizingViewController:(UIViewController *)delegate optionSelected:(NSArray *)json
{
    [self organizeWithJSON:json];
}

- (void)addTextToImage:(UILabel *)label {
    [label setUserInteractionEnabled:YES];
    [label setMultipleTouchEnabled:YES];
    
    UIRotationGestureRecognizer *rotate = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(handleRotate:)];
    [label addGestureRecognizer:rotate];
    
    UIPinchGestureRecognizer *pinch = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinch:)];
    [label addGestureRecognizer:pinch];
    
    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
    [label addGestureRecognizer:pan];
    
    UILongPressGestureRecognizer *touchAndHoldRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(touchAndHold:)];
    touchAndHoldRecognizer.minimumPressDuration = 0.3;
    [label addGestureRecognizer:touchAndHoldRecognizer];
    
    [imageDisplay addSubview:label];
}

- (UIDocumentInteractionController *)setupControllerWithURL:(NSURL*)fileURL usingDelegate:(id <UIDocumentInteractionControllerDelegate>)interactionDelegate
{
    UIDocumentInteractionController *interactionController = [UIDocumentInteractionController interactionControllerWithURL:fileURL];
    interactionController.delegate = self;
    
    return interactionController;
}

- (void)storeimage:(UIImage *)NewImg
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:@"InstaFix_Normal.ig"];
    NSData *imageData = UIImagePNGRepresentation(NewImg);
    [imageData writeToFile:savedImagePath atomically:NO];
}

@end
