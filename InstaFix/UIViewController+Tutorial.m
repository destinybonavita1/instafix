//
//  UIViewController+Tutorial.m
//  InstaFix
//
//  Created by Destiny Bonavita on 5/3/15.
//  Copyright (c) 2015 Young and Strong Productions. All rights reserved.
//

#import "TutorialViewController.h"
#import "UIViewController+Tutorial.h"

@implementation UIViewController (Tutorial)

- (void)displayTutorialWithMessage:(NSString *)message aboveView:(UIView *)view
{
    TutorialViewController *tut = [TutorialViewController initWithMessage:message];
    tut.modalPresentationStyle = UIModalPresentationPopover;
    tut.preferredContentSize = CGSizeMake(200, 85);
    
    UIPopoverPresentationController *pop = [tut popoverPresentationController];
    if (pop)
    {
        pop.delegate = tut;
        pop.sourceView = view;
        pop.sourceRect = CGRectZero;
        pop.permittedArrowDirections = UIPopoverArrowDirectionDown | UIPopoverArrowDirectionUp;
        [self presentViewController:tut animated:YES completion:nil];
    }
}

@end
