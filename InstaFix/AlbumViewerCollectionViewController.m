    //
//  AlbumViewerCollectionViewController.m
//  InstaFix
//
//  Created by Destiny Dawn on 12/27/14.
//  Copyright (c) 2014 Young and Strong Productions. All rights reserved.
//

#import <AssetsLibrary/AssetsLibrary.h>

#import "MISViewCell.h"
#import "AlbumViewerCollectionViewController.h"

@interface AlbumViewerCollectionViewController () <UICollectionViewDelegateFlowLayout>
{
    NSMutableArray *assets;
}

@property (nonatomic, retain) NSMutableDictionary *selectedImages;
@property (nonatomic, retain) UIBarButtonItem *doneButton;

@end

@implementation AlbumViewerCollectionViewController

static NSString * const reuseIdentifier = @"Cell";

#pragma mark - UIView Life Cycle

- (void)viewDidLoad {
    assets = [[NSMutableArray alloc] init];
    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = NO;
    [self styleView];
    
    // Register cell classes
    [self.collectionView registerClass:[MISViewCell class] forCellWithReuseIdentifier:reuseIdentifier];
    
    // Do any additional setup after loading the view.
    ALAssetsLibrary *al = [[ALAssetsLibrary alloc] init];
    [al groupForURL:self.groupURL resultBlock:^(ALAssetsGroup *group) {
        [group enumerateAssetsUsingBlock:^(ALAsset *result, NSUInteger index, BOOL *stop) {
            if (result && [[result valueForProperty:@"ALAssetPropertyType"] isEqualToString:@"ALAssetTypePhoto"] && [[[result defaultRepresentation] UTI] isEqualToString:@"com.compuserve.gif"] == FALSE) {
                NSMutableArray *keys = [[NSMutableArray alloc] init];
                [keys addObject:[UIImage imageWithCGImage:[result thumbnail]]];
                [keys addObject:[[result defaultRepresentation] url]];
                
                [assets addObject:keys];
            } else {
                [self.collectionView reloadData];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSInteger section = 0;
                    NSInteger item = [self collectionView:self.collectionView numberOfItemsInSection:section] - 1;
                    if (item < 0) {
                        return;
                    }
                    
                    NSIndexPath *lastIndexPath = [NSIndexPath indexPathForItem:item inSection:section];
                    [self.collectionView scrollToItemAtIndexPath:lastIndexPath atScrollPosition:UICollectionViewScrollPositionBottom animated:YES];
                });
            }
        }];
    } failureBlock:^(NSError *error) {
        //Handle error...
    }];
    
    //Sets the selected photos
    self.selectedImages = [NSMutableDictionary new];
    
    //Sets "done" button
    self.doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(finishedSelecting)];
    self.navigationItem.rightBarButtonItem = self.doneButton;
    self.doneButton.enabled = NO;
    
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private

- (void)styleView
{
    UIView *colBackgroundView = [[UIView alloc] initWithFrame:self.view.frame];
    colBackgroundView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.jpg"]];
    
    UIVisualEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
    UIVisualEffectView *effectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    effectView.frame = self.view.frame;
    
    [colBackgroundView addSubview:effectView];
    
    self.collectionView.backgroundView = colBackgroundView;
}

- (void)finishedSelecting
{
    if (self.delegate && self.selectedImages.count > 0)
        [self.delegate photoSelectorDelegate:self imagesSelected:self.selectedImages.allValues];
}

#pragma mark - <UICollectionViewDataSource>

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return assets.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    MISViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    UIImage *img = assets[indexPath.row][0];
    
    // Configure the cell
    cell.isSelected = (self.selectedImages[assets[indexPath.row][1]] != nil);
    [cell setPhoto:img];
    
    return cell;
}

#pragma mark - <UICollectionViewDelegate>

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.delegate)
    {
        NSURL *assetURL = assets[indexPath.row][1];
        if (self.selectedImages[assetURL])
        {
            [self.selectedImages removeObjectForKey:assetURL];
            [collectionView reloadData];
        }
        else
        {
            ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
            [library assetForURL:assetURL resultBlock:^(ALAsset *asset) {
                ALAssetRepresentation *resp = asset.defaultRepresentation;
                CGImageRef imgRef = resp.fullResolutionImage;
                UIImage *img = [UIImage imageWithCGImage:imgRef scale:resp.scale orientation:0];
                
                //Adds the image to the halo array
                self.selectedImages[assetURL] = img;
                self.doneButton.enabled = (self.selectedImages.count > 0);
                
                //Reloads the table view
                [collectionView reloadData];
            } failureBlock:^(NSError *error) {
                //Handle NSError.
            }];
        }
    }
    
    //Enables the button to do bad things
    self.doneButton.enabled = (self.selectedImages.count > 0);
    
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
}

#pragma mark - <UICollectionViewDelegateFlowLayout>

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(75, 75);
}

@end
