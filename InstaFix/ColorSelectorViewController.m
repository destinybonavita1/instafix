//
//  ColorSelectorViewController.m
//  InstaFix
//
//  Created by Destiny Dawn on 11/3/13.
//  Copyright (c) 2013 Young and Strong Productions. All rights reserved.
//

#import "RGBCell.h"
#import "Settings.h"
#import "ColorCell.h"
#import "ColorSelectorViewController.h"

@interface ColorSelectorViewController ()

@property (nonatomic, retain) NSUserDefaults *sharedDefaults;

@end

@implementation ColorSelectorViewController
@synthesize colorView, rgbSegmented;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    typeOf = @"Red";

    self.sharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.fadingeclipse.instafix"];
    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(hideView)];
    [swipe setDirection:UISwipeGestureRecognizerDirectionDown];
    [[self view] addGestureRecognizer:swipe];
    
    //Adds notification listeners
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tutorialPopoverFinished:) name:kTutorialPopoverDimissed object:nil];
}

- (void)hideView {
    [self hideView:YES];
}

- (void)hideView:(BOOL)remove {
    [UIView animateWithDuration:0.5f animations:^(void) {
        CGRect frame = self.view.frame;
        frame.origin.y += [UIScreen mainScreen].bounds.size.height;
        
        self.view.alpha = 0.0f;
        self.view.frame = frame;
    } completion:^(BOOL finished) {
        if ([self.delegate respondsToSelector:@selector(ViewHidden)]) {
            [self.delegate ViewHidden];
        }
        
        if (remove) [self.view removeFromSuperview];
    }];
}

- (void)showView {
    [UIView animateWithDuration:0.5f animations:^(void) {
        CGRect frame = self.view.frame;
        frame.origin.y -= [UIScreen mainScreen].bounds.size.height;
        
        self.view.alpha = 0.0f;
        self.view.frame = frame;
    } completion:^(BOOL finished) {
        [self showReport];
    }];
}

- (IBAction)changeRGB:(id)sender {
    UISegmentedControl *con = (UISegmentedControl *)sender;
    typeOf = [con titleForSegmentAtIndex:[con selectedSegmentIndex]];
    
    [self setColorViewChoices];
}

- (void)setColorViewChoices {
    UISegmentedControl *con = (UISegmentedControl *)rgbSegmented;
    typeOf = [con titleForSegmentAtIndex:[con selectedSegmentIndex]];
    for (UIView *v in [colorView subviews]) {
        [v removeFromSuperview];
    }
    
    int pos = 0.0f;
    if ([typeOf isEqualToString:@"Standard"]) {
        CGFloat R = 0.0f, G = 0.0f, B = 0.0f;
        
        //White
        for (float i = 0.8f; i < 2.8f; i += 0.4f) {
            //Sets the new RGB
            R = (255.0f / (255.0f * i));
            G = (255.0f / (255.0f * i));
            B = (255.0f / (255.0f * i));
            
            //Creates the color cell
            UIColor *color = [UIColor colorWithRed:R green:G blue:B alpha:1.0f];
            [self addCellForFrame:CGRectMake(15 + ((pos - 0.0f) * (80 + 15)), 15, 80, 80) color:color];
            
            pos++;
        }
        
        //Red
        for (float i = 0.8f; i < 2.8f; i += 0.4f) {
            G = 0.0f;
            R = (255.0f / 255.0f);
            B = (255.0f / (255.0f * i));
            
            //Creates the color cell
            UIColor *color = [UIColor colorWithRed:R green:G blue:B alpha:1.0f];
            [self addCellForFrame:CGRectMake(15 + ((pos - 0.0f) * (80 + 15)), 15, 80, 80) color:color];
            
            pos++;
        }
        
        for (float i = 2.8f; i > 0.8f; i -= 0.4f) {
            B = 0.0f;
            R = (255.0f / 255.0f);
            G = (255.0f / (255.0f * i));
            
            //Creates the color cell
            UIColor *color = [UIColor colorWithRed:R green:G blue:B alpha:1.0f];
            [self addCellForFrame:CGRectMake(15 + ((pos - 0.0f) * (80 + 15)), 15, 80, 80) color:color];
            
            pos++;
        }
        
        //Green
        for (float i = 0.8f; i < 2.8f; i += 0.4f) {
            B = 0.0f;
            G = (255.0f / 255.0f);
            R = (255.0f / (255.0f * i));
            
            //Creates the color cell
            UIColor *color = [UIColor colorWithRed:R green:G blue:B alpha:1.0f];
            [self addCellForFrame:CGRectMake(15 + ((pos - 0.0f) * (80 + 15)), 15, 80, 80) color:color];
            
            pos++;
        }
        
        //Blue
        for (float i = 0.8f; i < 2.8f; i += 0.4f) {
            R = 0.0f;
            B = (255.0f / 255.0f);
            G = (255.0f / (255.0f * i));
            
            //Creates the color cell
            UIColor *color = [UIColor colorWithRed:R green:G blue:B alpha:1.0f];
            [self addCellForFrame:CGRectMake(15 + ((pos - 0.0f) * (80 + 15)), 15, 80, 80) color:color];
            
            pos++;
        }
    }
    else if ([typeOf isEqualToString:@"Favorites"]) {
        NSMutableArray *colors = self.favoriteBackgrounds;
        if (colors.count == 0)
        {
            ColorCell *cell = [ColorCell colorCellWithFrame:CGRectMake(15 + ((pos - 0.0f) * (80 + 15)), 15, 80, 80) color:[UIColor clearColor]];
            cell.isNoFavorites = YES;
            cell.userInteractionEnabled = NO;
            
            [colorView addSubview:cell];
            
            //Increments the position
            pos++;
        }
        else
        {
            for (UIColor *color in colors) {
                //Adds the favorites
                [self addCellForFrame:CGRectMake(15 + ((pos - 0.0f) * (80 + 15)), 15, 80, 80) color:color];
                
                pos++;
            }
        }
    }
    else if ([typeOf isEqualToString:@"Custom"]) {
        //Creates the frame
        CGRect frame = CGRectMake(0, 0, CGRectGetWidth(self.colorView.frame), CGRectGetHeight(self.colorView.frame));
        
        //Adds the RGB Cell
        RGBCell *rgb = [[RGBCell alloc] initWithFrame:frame];
        rgb.superController = self;
        
        [rgb setDelegate:^void(CGFloat r, CGFloat g, CGFloat b, CGFloat alpha) {
            [self.delegate colorChange:[UIColor colorWithRed:r green:g blue:b alpha:alpha]];
        }];
        
        [colorView addSubview:rgb];
        
        if (![Settings instance].tutorialColorGrabbing) {
            [Settings instance].tutorialColorGrabbing = YES;
            [self.parentViewController displayTutorialWithMessage:@"Press this to press grab a color from your picture." aboveView:rgb.colorView];
        }
    }
    
    colorView.scrollEnabled = !([typeOf isEqualToString:@"Custom"]);
    [colorView setContentSize:CGSizeMake((80 + 16) * pos, (80 + 15))];
}

typedef void (^CellCompletionBlock)(ColorCell *cell);

- (void)addCellForFrame:(CGRect)frame color:(UIColor *)color
{
    [self addCellForFrame:frame color:color completion:nil];
}

- (void)addCellForFrame:(CGRect)frame color:(UIColor *)color completion:(CellCompletionBlock)completion
{
    ColorCell *cell = [ColorCell colorCellWithFrame:frame color:color];
    cell.superController = self;
    
    [colorView addSubview:cell];
    
    if (completion)
        completion(cell);
}

- (NSMutableArray *)favoriteBackgrounds {
    NSMutableArray *colors = [NSKeyedUnarchiver unarchiveObjectWithData:[self.sharedDefaults valueForKey:@"favorites"]];
    if (!colors)
    {
        colors = [[NSMutableArray alloc] init];
        [self setFavoriteBackgrounds:colors];
    }
    
    return colors;
}

- (void)setFavoriteBackgrounds:(NSMutableArray *)colors {
    [self.sharedDefaults setValue:[NSKeyedArchiver archivedDataWithRootObject:colors] forKey:@"favorites"];
    [self.sharedDefaults synchronize];
    
    if ([typeOf isEqualToString:@"Favorites"]) {
        [self setColorViewChoices];
    }
}

- (void)showInView:(UIView *)view aboveView:(UIView *)above
{
    //Gets the main frame
    CGRect main = [UIScreen mainScreen].bounds;
    
    //Creates the view
    UIView *v = self.view;
    v.alpha = 0.0f;
    v.frame = CGRectMake(0.0f, CGRectGetHeight(main), CGRectGetWidth(main), 130);
    
    //Handles auto resizing if it's an iPad
    if ([[[UIDevice currentDevice] model] hasPrefix:@"iPad"])
        v.autoresizingMask = UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleWidth;
    
    //Sets the colors
    [self setColorViewChoices];
    
    //Inserts the subview above everything
    [view insertSubview:v aboveSubview:above];
    
    //Animates showing the view
    [UIView animateWithDuration:0.5 animations:^(void) {
        CGRect frame = v.frame;
        frame.origin.y -= frame.size.height;
        
        v.frame = frame;
        v.alpha = 1.0f;
    } completion:^(BOOL finished) {
        [self showReport];
    }];
}

- (void)showReport
{
    NSMutableDictionary *settings = [[Settings instance].tutorialSwipeClose mutableCopy];
    if (!settings[@"ColorView"]) {
        settings[@"ColorView"] = @(0);
        
        //Displays the message
        [self displayTutorialWithMessage:@"Swipe down this view to close it." aboveView:self.view];
        
        //Saves the settings
        [Settings instance].tutorialSwipeClose = settings;
    }
}

- (void)tutorialPopoverFinished:(NSNotification *)notification
{
    if (![Settings instance].tutorialBasicColoring) {
        [Settings instance].tutorialBasicColoring = YES;
        [self.parentViewController displayTutorialWithMessage:@"Press and hold on any color to add to favorites." aboveView:self.view];
    }
}

@end
