//
//  TutorialViewController.h
//  InstaFix
//
//  Created by Destiny Bonavita on 5/3/15.
//  Copyright (c) 2015 Young and Strong Productions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TutorialViewController : UIViewController <UIPopoverPresentationControllerDelegate>

+ (instancetype)initWithMessage:(NSString *)message;

@end
