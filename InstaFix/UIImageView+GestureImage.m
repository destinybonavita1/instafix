//
//  UIImageView+GestureImage.m
//  InstaFix
//
//  Created by Destiny Eclipse on 3/14/14.
//  Copyright (c) 2014 Young and Strong Productions. All rights reserved.
//

#import "UIImageView+GestureImage.h"

@interface GestureImage ()

@end

@implementation GestureImage

- (void)setGenerator
{
    if (!CGAffineTransformIsIdentity(self.transform)) {
        CGSize size = self.image.size;
        
        CGSize square = [[ImageHandler instance] sizeToSquare:size];
        square.width += square.width * 0.2;
        square.height = square.width;
        
        CGFloat angle = atan2(self.transform.b, self.transform.a);
        
        UIGraphicsBeginImageContext(square);
        CGContextRef context = UIGraphicsGetCurrentContext();
        
        //Translates it to another lanauge and then rotates it in frustration
        CGContextTranslateCTM(context, square.width * 0.5f, square.height * 0.5f);
        CGContextRotateCTM(context, angle);
        
        //Draws the image
        [self.image drawInRect:CGRectMake(-size.width * 0.5f, -size.height * 0.5f, size.width, size.height)];
        
        //Saves it like it'll actually work 😂
        UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        self.rotatedImage = img;
    }
    else
        self.rotatedImage = self.image;
}

- (CGRect)boundingFrame {
    CGFloat angleOfRotation = RadiansToDegrees(atan2(self.transform.b, self.transform.a));
    // Calculate the width and height of the bounding rectangle using basic trig
    CGFloat newWidth = self.frame.size.width * fabs(cosf(angleOfRotation)) + self.frame.size.height * fabs(sinf(angleOfRotation));
    CGFloat newHeight = self.frame.size.height * fabs(cosf(angleOfRotation)) + self.frame.size.width * fabs(sinf(angleOfRotation));
    
    // Calculate the position of the origin
    CGFloat newX = self.frame.origin.x + ((self.frame.size.width - newWidth) / 2);
    CGFloat newY = self.frame.origin.y + ((self.frame.size.height - newHeight) / 2);
    
    // Return the rectangle
    return CGRectMake(newX, newY, newWidth, newHeight);
}

@end
