//
//  RGBCell.h
//  InstaFix
//
//  Created by Destiny Bonavita on 5/3/15.
//  Copyright (c) 2015 Young and Strong Productions. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ColorSelectorViewController;

typedef void (^RGBCellChangedBlock)(CGFloat r, CGFloat g, CGFloat b, CGFloat alpha);

@interface RGBCell : UIView

- (id)initWithFrame:(CGRect)frame;

@property (nonatomic, weak) IBOutlet UIView *colorView;
@property (nonatomic, weak) IBOutlet UISlider *redSlider;
@property (nonatomic, weak) IBOutlet UISlider *blueSlider;
@property (nonatomic, weak) IBOutlet UISlider *greenSlider;
@property (nonatomic, weak) IBOutlet UILabel *toggleLabel;

@property (nonatomic, retain) ColorSelectorViewController *superController;

- (IBAction)sliderValueChanged:(id)sender;

- (void)tapColor:(UITapGestureRecognizer *)tap;
- (void)setDelegate:(RGBCellChangedBlock)delegate;

@end
