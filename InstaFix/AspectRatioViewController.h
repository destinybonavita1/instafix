//
//  AspectRatioViewController.h
//  InstaFix
//
//  Created by Destiny Eclipse on 3/14/14.
//  Copyright (c) 2014 Young and Strong Productions. All rights reserved.
//

#import <UIKit/UIKit.h>
 

@protocol AspectRatioSelectorDelegate <NSObject>

@optional
- (void)aspectRatioControllerViewHidden:(UIViewController *)aspectRatioController;
- (void)aspectRatioController:(UIViewController *)aspectRatioController ratio:(float)ratio;

@end

@interface AspectRatioViewController : UIViewController <UIGestureRecognizerDelegate> {
    NSString *typeOf;
}

@property (weak, nonatomic) IBOutlet UIScrollView *viewScrollView;
@property (nonatomic, retain) id<AspectRatioSelectorDelegate> delegate;
@property (weak, nonatomic) IBOutlet UISegmentedControl *orientationSelection;

- (void)SetAspectRatioOptions;
- (IBAction)didChangeOrientation:(id)sender;

@end

@interface AspectView : UIView

@property (nonatomic, readwrite) float AspectRatio;

@end