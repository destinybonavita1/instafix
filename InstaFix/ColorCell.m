//
//  ColorCell.m
//  InstaFix
//
//  Created by Destiny Bonavita on 4/27/15.
//  Copyright (c) 2015 Young and Strong Productions. All rights reserved.
//

#import "ColorCell.h"
#import "ImageHandler.h"
#import "UIView+ColorView.h"
#import "ColorSelectorViewController.h"

#import <QuartzCore/QuartzCore.h>

@interface ColorCell ()

@property (nonatomic, retain) UILabel *label;

@end

@implementation ColorCell

+ (id)colorCellWithFrame:(CGRect)frame color:(UIColor *)color
{
    ColorCell *cell = [[ColorCell alloc] initWithFrame:frame];
    
    //Adds main objects
    [cell addLabel];
    [cell addGestureRecognizers];
    
    //Creates the RGB colors
    const CGFloat *components = CGColorGetComponents(color.CGColor);
    CGFloat R = components[0];
    CGFloat G = components[1];
    CGFloat B = components[2];
    UIColor *notColor = [UIColor colorWithRed:(1 - (255.0f / R)) green:(1 - (255.0f / G)) blue:(1 - (255.0f / B)) alpha:1.0f];
    
    //Styles the cell
    cell.backgroundColor = color;
    cell.layer.borderWidth = 1.0f;
    cell.layer.borderColor = [UIColor blackColor].CGColor;
    
    //Sets the color for the label
    if (color == [UIColor clearColor])
        cell.label.text = @"Clear\nColor";
    cell.label.textColor = notColor;
    
    return cell;
}

+ (id)colorCellWithFrame:(CGRect)frame R:(CGFloat)R G:(CGFloat)G B:(CGFloat)B
{
    ColorCell *cell = [[ColorCell alloc] initWithFrame:frame];
    
    //Adds main objects
    [cell addLabel];
    [cell addGestureRecognizers];
    
    //Creates the RGB colors
    UIColor *color = [UIColor colorWithRed:R green:G blue:B alpha:1.0f];
    UIColor *notColor = [UIColor colorWithRed:(1 - (255.0f / R)) green:(1 - (255.0f / G)) blue:(1 - (255.0f / B)) alpha:1.0f];
    
    //Styles the cell
    cell.backgroundColor = color;
    cell.layer.borderWidth = 1.0f;
    cell.layer.borderColor = [UIColor blackColor].CGColor;
    
    //Sets the color for the label
    cell.label.textColor = notColor;
    
    return cell;
}

#pragma mark - Properties

- (void)setIsNoFavorites:(BOOL)isNoFavorites
{
    if (_isNoFavorites != isNoFavorites) {
        isNoFavorites = isNoFavorites;
        
        //Changes the label
        self.label.text = @"None\nSaved";
    }
}

#pragma mark - Private

- (void)addLabel
{
    //Adds the label
    self.label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.height, self.frame.size.width)];
    self.label.text = @"Press\nme";
    self.label.numberOfLines = MAXFLOAT;
    self.label.textAlignment = NSTextAlignmentCenter;
    self.label.backgroundColor = [UIColor clearColor];
    
    [self addSubview:self.label];
}

- (void)addGestureRecognizers
{
    //Creates Gestures
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pushColor:)];
    UILongPressGestureRecognizer *longGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(saveColor:)];
    
    //Adds gestures
    [self addGestureRecognizer:gesture];
    [self addGestureRecognizer:longGesture];
}

#pragma mark - Gesture Handlers

- (void)pushColor:(UITapGestureRecognizer *)tap
{
    ColorSelectorViewController *s = (ColorSelectorViewController *)self.superController;
    
    [s.delegate colorChange:tap.view.backgroundColor];
}

- (void)saveColor:(UILongPressGestureRecognizer *)longPress
{
    if (longPress.state == UIGestureRecognizerStateBegan)
    {
        ColorSelectorViewController *s = (ColorSelectorViewController *)self.superController;
        
        UIView *view = longPress.view;
        NSMutableArray *colors = s.favoriteBackgrounds;
        
        if (![colors containsObject:view.backgroundColor]) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"InstaFix" message:@"What would you like to do with this color?" preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
            [alert addAction:[UIAlertAction actionWithTitle:@"Add to Favorites" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                [colors addObject:view.backgroundColor];
                [s setFavoriteBackgrounds:colors];
            }]];
            
            [s.view.window.rootViewController presentViewController:alert animated:YES completion:nil];
        }
        else {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"InstaFix" message:@"What would you like to do with this color?" preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
            [alert addAction:[UIAlertAction actionWithTitle:@"Remove from Favorites" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                [colors removeObject:view.backgroundColor];
                [s setFavoriteBackgrounds:colors];
            }]];
            
            [s.view.window.rootViewController presentViewController:alert animated:YES completion:nil];
        }
    }
}

- (void)tapColor:(UITapGestureRecognizer *)tap
{
    //Gets the color
    ColorSelectorViewController *s = (ColorSelectorViewController *)self.superController;
    UIColor *color = [tap.view colorOfPoint:[tap locationInView:tap.view]];
    
    //Add color to favorites for later
    [s.favoriteBackgrounds addObject:color];
    
    //Pushes the delegate and removes recognizer
    [s.delegate colorChange:color];
    [tap.view removeGestureRecognizer:tap];
    
    //Sets label
    self.label.text = @"Tap\nMe";
}

@end
