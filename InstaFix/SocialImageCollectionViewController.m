//
//  SocialImageCollectionViewController.m
//  InstaFix
//
//  Created by Destiny Dawn on 12/27/14.
//  Copyright (c) 2014 Young and Strong Productions. All rights reserved.
//

#import "Settings.h"
#import "Facebook.h"
#import "Instagram.h"
#import "MISViewCell.h"
#import "SocialImageCollectionViewController.h"

@interface SocialImageCollectionViewController () <UICollectionViewDelegateFlowLayout>

@property (nonatomic) BOOL ready;
@property (nonatomic, retain) NSMutableArray *images;

@end

@implementation SocialImageCollectionViewController

static NSString * const reuseIdentifier = @"Cell";

#pragma mark - Public

+ (instancetype)instance:(SocialNetwork)network
{
    if (network == FacebookNet)
    {
        static SocialImageCollectionViewController *f;
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            f = [[SocialImageCollectionViewController alloc] initWithCollectionViewLayout:[self flowLayout]];
            f.network = network;
        });
        
        return f;
    }
    else if (network == InstagramNet)
    {
        static SocialImageCollectionViewController *i;
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            i = [[SocialImageCollectionViewController alloc] initWithCollectionViewLayout:[self flowLayout]];
            i.network = network;
        });
        
        return i;
    }
    
    return nil;
}

+ (UICollectionViewFlowLayout *)flowLayout
{
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.minimumLineSpacing = 0.0f;
    layout.minimumInteritemSpacing = 0.0f;
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    
    return layout;
}

#pragma mark - UIView Life Cycle

- (void)viewDidLoad {
    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = NO;
    self.images = [[NSMutableArray alloc] init];
    [self styleView];
    
    // Register cell classes
    [self.collectionView registerClass:[MISViewCell class] forCellWithReuseIdentifier:reuseIdentifier];
    
    // Do any additional setup after loading the view.
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [self downloadImages:self.urls];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if ([Settings instance].autoLoad)
            {
                if (self.network == FacebookNet)
                    [self loadFacebookCursor];
                else if (self.network == InstagramNet)
                    [self loadInstagramCursor];
            }
        });
    });
    
    [super viewDidLoad];
}

static bool r = false;
- (void)viewDidAppear:(BOOL)animated
{
    if (self.network && !r)
    {
        r = YES;
        
        NSMutableArray *ar = [self.navigationController.viewControllers mutableCopy];
        [ar removeObjectAtIndex:[self.navigationController.viewControllers indexOfObject:self] - 1];
        [self.navigationController setViewControllers:ar];
    }
    
    [super viewDidAppear:animated];
}

- (void)styleView
{
    UIView *colBackgroundView = [[UIView alloc] initWithFrame:self.view.frame];
    colBackgroundView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Background.jpg"]];
    
    UIVisualEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
    UIVisualEffectView *effectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    effectView.frame = self.view.frame;
    
    [colBackgroundView addSubview:effectView];
    
    self.collectionView.backgroundView = colBackgroundView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Image Handling

- (void)downloadImages:(NSArray *)urls
{
    for (id imgs in urls)
    {
        NSDictionary *lowest = (self.network == FacebookNet) ? [imgs lastObject] : imgs[@"thumbnail"];
        NSString *url = (self.network == FacebookNet) ? lowest[@"source"] : lowest[@"url"];
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
        UIImage *img = [UIImage imageWithData:data];
        if (img) [self.images addObject:img];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.collectionView reloadData];
        });
    }
    
    self.ready = YES;
}

- (UIImage *)downloadImage:(NSIndexPath *)path
{
    id imgAr = self.urls[path.row];
    NSDictionary *lowest = (self.network == FacebookNet) ? [imgAr firstObject] : imgAr[@"standard_resolution"];
    NSString *url = (self.network == FacebookNet) ? lowest[@"source"] : lowest[@"url"];;
    NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
    return [UIImage imageWithData:data];
}

#pragma mark - <UICollectionViewDataSource>

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return (self.cursor) ? self.images.count + 1 : self.images.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    MISViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    // Configure the cell
    if (indexPath.row == self.images.count)
        cell.isLoadMore = YES;
    else
    {
        UIImage *img = self.images[indexPath.row];
        cell.isLoadMore = NO;
        
        [cell setPhoto:img];
    }
    
    return cell;
}

#pragma mark - <UICollectionViewDelegate>

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    MISViewCell *cell = (MISViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    if (self.delegate && !cell.isLoadMore)
    {
        [self.delegate photoSelectorDelegate:self imageSelected:[self downloadImage:indexPath]];
    }
    else if (cell.isLoadMore && self.ready)
    {
        self.ready = NO;
        
        if (self.network == FacebookNet) [self loadFacebookCursor];
        else if (self.network == InstagramNet) [self loadInstagramCursor];
    }
    
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
}

#pragma mark - <UICollectionViewDelegateFlowLayout>

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(75, 75);
}

#pragma mark - Social

- (void)loadInstagramCursor
{
    NSDictionary *photosJSON = [[Instagram instance] photosFromAccount:self.cursor];
    NSArray *photosData = photosJSON[@"data"];
    NSMutableArray *photoUrls = [[NSMutableArray alloc] init];
    for (NSDictionary *photo in photosData)
        [photoUrls addObject:photo[@"images"]];
    [self.urls addObjectsFromArray:photoUrls];
    
    NSDictionary *pagaton = photosJSON[@"pagination"];
    self.cursor = pagaton[@"next_url"];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [self downloadImages:photoUrls];
        
        if ([Settings instance].autoLoad && self.cursor)
            [self loadInstagramCursor];
    });
}

- (void)loadFacebookCursor
{
    [FBRequestConnection startWithGraphPath:self.cursor parameters:nil HTTPMethod:@"GET" completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        FBGraphObject *returned = result;
        if (returned)
        {
            NSArray *photos = returned.allValues[0];
            NSString *cursor = returned.allValues[1][@"next"];
            if (cursor)
                cursor = [cursor stringByReplacingOccurrencesOfString:@"https://graph.facebook.com/v2.2/" withString:@""];
            self.cursor = cursor;
            
            NSMutableArray *photoUrls = [[NSMutableArray alloc] init];
            for (NSDictionary *dict in photos)
            {
                [photoUrls addObject:dict[@"images"]];
            }
            
            [self.urls addObjectsFromArray:photoUrls];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                [self downloadImages:photoUrls];
                
                if ([Settings instance].autoLoad && self.cursor)
                    [self loadFacebookCursor];
            });
        }
    }];
}

@end
