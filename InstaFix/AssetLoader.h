//
//  AssetLoader.h
//  InstaFix
//
//  Created by Destiny Dawn on 11/26/13.
//  Copyright (c) 2013 Young and Strong Productions. All rights reserved.
//

#import "ImageHandler.h"
#import <AssetsLibrary/AssetsLibrary.h>

@interface AssetLoader : ALAssetsLibrary

@property (nonatomic, retain) NSNumber *assetCount;

+ (instancetype)sharedInstance;
- (UIImage *)loadAsset:(NSURL *)assetURL;
- (UIImage *)loadThumbnail:(NSURL *)assetURL;

@end
