//
//  ResizingViewController.m
//  InstaFix
//
//  Created by Destiny Bonavita on 5/6/15.
//  Copyright (c) 2015 Young and Strong Productions. All rights reserved.
//

#import "Settings.h"
#import "ResizeView.h"
#import "ResizingViewController.h"

@interface ResizingViewController ()

@property (nonatomic, weak) IBOutlet UIScrollView *scrollView;

@end

@implementation ResizingViewController

#pragma mark - UIView Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(closeView)];
    swipe.direction = UISwipeGestureRecognizerDirectionDown;
    
    [self.view addGestureRecognizer:swipe];
    
    //Adds notification listeners
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tutorialPopoverFinished:) name:kTutorialPopoverDimissed object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Public

- (void)showInView:(UIView *)view aboveView:(UIView *)above
{
    //Gets the main frame
    CGRect main = [UIScreen mainScreen].bounds;
    
    //Creates the view
    UIView *v = self.view;
    v.alpha = 0.0f;
    v.frame = CGRectMake(0.0f, CGRectGetHeight(main), CGRectGetWidth(main), 100);
    
    //Handles auto resizing if it's an iPad
    if ([[[UIDevice currentDevice] model] hasPrefix:@"iPad"])
        v.autoresizingMask = UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleWidth;
    
    //Sets the views
    [self setViews];
    
    //Inserts the subview above everything
    [view insertSubview:v aboveSubview:above];
    
    //Animates showing the view
    [UIView animateWithDuration:0.5 animations:^(void) {
        CGRect frame = v.frame;
        frame.origin.y -= frame.size.height;
        
        v.frame = frame;
        v.alpha = 1.0f;
    } completion:^(BOOL finished) {
        [self setViews];
        
        NSMutableDictionary *settings = [[Settings instance].tutorialSwipeClose mutableCopy];
        if (!settings[@"ResizingView"]) {
            settings[@"ResizingView"] = @(0);
            
            //Displays the message
            [self displayTutorialWithMessage:@"Swipe down this view to close it." aboveView:self.view];
            
            //Saves the settings
            [Settings instance].tutorialSwipeClose = settings;
        }
    }];
}

#pragma mark - Private

- (void)setViews
{
    //Empty the scroll view
    for (UIView *view in self.scrollView.subviews)
        [view removeFromSuperview];
    
    //Creates content size
    CGSize contentSize = CGSizeZero;
    
    //Sets the frames
    CGFloat padding = 15.0f;
    CGFloat height = CGRectGetHeight(self.scrollView.frame) - padding;
    CGRect frame = CGRectMake(padding, padding / 2.0f, height, height);
    
    //Sets the positioning
    int items = 0.0f;
    
    //Adds the subviews
    for (NSArray *views in self.json) {
        frame.origin.x = (items == 0) ? padding : (height + (padding * 2.0f)) * items;
        
        //Creates new view
        ResizeView *view = [[ResizeView alloc] initWithFrame:frame];
        view.layer.borderColor = [UIColor blackColor].CGColor;
        view.userInteractionEnabled = YES;
        view.layer.borderWidth = 1.0f;
        view.json = views;
        
        //Sets the recognizer
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(resizeViewTapped:)];
        [view addGestureRecognizer:tap];
        
        //Adds the subviews to the view
        for (NSDictionary *dic in views) {
            CGRect f = CGRectMake(0, 0, 0, 0);
            f.origin = CGPointMake(frame.size.width * [dic[@"left"] floatValue], frame.size.height * [dic[@"top"] floatValue]);
            f.size = CGSizeMake(frame.size.width * [dic[@"width"] floatValue], frame.size.height * [dic[@"height"] floatValue]);
            
            UIView *v = [[UIView alloc] initWithFrame:f];
            v.layer.borderColor = [UIColor blackColor].CGColor;
            v.layer.borderWidth = 1.0f;
            v.backgroundColor = [UIColor whiteColor];
            
            //Sets the content size
            [view addSubview:v];
        }
        
        [self.scrollView addSubview:view];
        items++;
    }
    
    //Sets the content size
    contentSize.width = (height + (padding * 2.0f)) * items;
    self.scrollView.contentSize = contentSize;
}

- (void)tutorialPopoverFinished:(NSNotification *)notification
{
    
}

- (void)closeView
{
    [UIView animateWithDuration:0.5f animations:^(void) {
        CGRect frame = self.view.frame;
        frame.origin.y += [UIScreen mainScreen].bounds.size.height;
        
        self.view.alpha = 0.0f;
        self.view.frame = frame;
    } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
    }];
}

#pragma mark - UIGestureRecognizers

- (void)resizeViewTapped:(UITapGestureRecognizer *)recognizer
{
    if (self.delegate)
    {
        ResizeView *resize = (ResizeView *)recognizer.view;
        
        [self.delegate resizingViewController:self optionSelected:resize.json];
    }
}

@end
