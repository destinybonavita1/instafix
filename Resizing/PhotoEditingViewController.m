//
//  PhotoEditingViewController.m
//  Resizing
//
//  Created by Destiny Dawn on 11/25/14.
//  Copyright (c) 2014 Young and Strong Productions. All rights reserved.
//

#import <Photos/Photos.h>
#import <PhotosUI/PhotosUI.h>

#import "ImageHandler.h"
#import "PhotoEditingViewController.h"
#import "ColorSelectorViewController.h"

@interface PhotoEditingViewController () <PHContentEditingController, ColorSelectorDelegate>

@property (strong) PHContentEditingInput *input;
@property (weak, nonatomic) IBOutlet UIToolbar *toolBar;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (retain, nonatomic) ColorSelectorViewController *colorSelector;

@end

@implementation PhotoEditingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //Gets the Color Selector
    self.colorSelector = [[UIStoryboard storyboardWithName:@"MainInterface" bundle:nil] instantiateViewControllerWithIdentifier:@"ColorSelector"];
    
    //Sets it's properties
    self.colorSelector.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - PHContentEditingController

- (BOOL)canHandleAdjustmentData:(PHAdjustmentData *)adjustmentData {
    // Inspect the adjustmentData to determine whether your extension can work with past edits.
    // (Typically, you use its formatIdentifier and formatVersion properties to do this.)
    return NO;
}

- (void)startContentEditingWithInput:(PHContentEditingInput *)contentEditingInput placeholderImage:(UIImage *)placeholderImage {
    // Present content for editing, and keep the contentEditingInput for use when closing the edit session.
    // If you returned YES from canHandleAdjustmentData:, contentEditingInput has the original image and adjustment data.
    // If you returned NO, the contentEditingInput has past edits "baked in".
    self.input = contentEditingInput;
    self.imageView.image = [ImageHandler resizeImage:self.input.displaySizeImage color:[UIColor whiteColor]];
}

- (void)finishContentEditingWithCompletionHandler:(void (^)(PHContentEditingOutput *))completionHandler {
    // Update UI to reflect that editing has finished and output is being rendered.
    
    // Render and provide output on a background queue.
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // Create editing output from the editing input.
        PHContentEditingOutput *output = [[PHContentEditingOutput alloc] initWithContentEditingInput:self.input];
        
        NSData *image = UIImageJPEGRepresentation(self.imageView.image, 1.0f);
        PHAdjustmentData *adjustmentData = [[PHAdjustmentData alloc] initWithFormatIdentifier:@"com.youngandstrong.instafix" formatVersion:@"1.0" data:image];
        output.adjustmentData = adjustmentData;
        
        [image writeToURL:output.renderedContentURL atomically:YES];
        
        completionHandler(output);
    });
}

- (BOOL)shouldShowCancelConfirmation {
    // Returns whether a confirmation to discard changes should be shown to the user on cancel.
    // (Typically, you should return YES if there are any unsaved changes.)
    return NO;
}

- (void)cancelContentEditing {
    // Clean up temporary files, etc.
    // May be called after finishContentEditingWithCompletionHandler: while you prepare output.
}

#pragma mark - IBActions

- (IBAction)didSwipeToolbar:(UISwipeGestureRecognizer *)recognizer {
    [self.colorSelector showInView:self.view aboveView:self.imageView];
}

#pragma mark - ColorSelector Delegate

- (void)colorChange:(UIColor *)color {
    self.imageView.image = [ImageHandler resizeImage:self.input.displaySizeImage color:color];
}

@end
